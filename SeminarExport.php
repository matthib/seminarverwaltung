<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
 * Class SeminarExport
 *
 */
class SeminarExport extends Backend
{


	/**
	 * Return a form to choose a CSV file and import it
	 * @param object
	 * @return string
	 */
	public function export(DataContainer $dc)
	{
		if ($this->Input->get('key') == 'seminarexport') {
			// get records
			$arrExport = array();
			$objRow = $this->Database->prepare("SELECT id,intern FROM tl_seminar_events WHERE 1 ORDER by id")
						->execute();
	
			while ($objRow->next())
			{
				$arrExport[] = $objRow->row();			
			}
	
			// start output
			$exportFile =  'export_seminar_' . date("Ymd-Hi");
			
			header('Content-Type: application/csv');
			header('Content-Transfer-Encoding: binary');
			header('Content-Disposition: attachment; filename="' . $exportFile .'.csv"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');
			
			$output = '';
			$output .= 'id;kursnr'. "\n" ;
	
			foreach ($arrExport as $export) 
			{
				$output .= join(';', $export). "\n";
			}
	
			echo $output;
			exit;
		} else if ($this->Input->get('key') == 'bookingexport') {
			// get records
			$arrExport = array();
			// id	anrede	name	vorname	strasse	plz	ort	email	telefon	buch	mitteilung	am_datum	kurs_fk
			$objRow = $this->Database->prepare("SELECT id,gender,lastname,firstname,street,postal,city,email,phone,remark,booking_date,pid,intern,cost FROM tl_seminar_booking WHERE 1 ORDER by id")
						->execute();
	
			while ($objRow->next())
			{
				$objCat = $this->Database->prepare("SELECT title FROM tl_seminar_category WHERE id=(SELECT pid FROM tl_seminar WHERE id=(SELECT pid FROM tl_seminar_events WHERE id=?))")->limit(1)->execute($objRow->pid);
				if ($objCat->numRows) {
					$arrExp['category'] = $objCat->title;
				} else {
					$arrExp['category'] = ' ';
				}
				$arrExport[] = array_merge($objRow->row(),$arrExp);
			}
	
			// start output
			$exportFile =  'export_booking_' . date("Ymd-Hi");
			
			header('Content-Type: application/csv');
			header('Content-Transfer-Encoding: binary');
			header('Content-Disposition: attachment; filename="' . $exportFile .'.csv"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');
			
			$output = '';
			$output .= '"buchungs_id";"anrede";"name";"vorname";"strasse";"plz";"ort";"email";"telefon";"mitteilung";"buchungsdatum";"event_id";"interne_event_nr";"cost";"kategorie"'."\n"; //'"id","kursnr"'. "\n" ;
	
			foreach ($arrExport as $export) 
			{
				foreach($export as $k=>$v) {
					$data = 'NULL';
				    if ($k == 'gender') { 
				    	if ($v == 'm') {
					        $data = 'Herr';
				    	} else if ($v == 'w') {
					        $data = 'Frau';
				    	} else {
					    	$data = 'NULL';
				    	}
				    } else if ($k == 'remark') {
					    $var = str_replace("\n"," ",strip_tags(html_entity_decode($v,ENT_NOQUOTES,"ISO8859-1")));
					    $var = str_replace("&#40;","(",$var);
					    $var = str_replace("&#41;",")",$var);
					    $data = $var;
				    } else if ($k == 'booking_date') {
					    $data = date('d.m.Y H:i',$v);
				    } else {
					    $data = html_entity_decode($v,ENT_NOQUOTES,"ISO8859-1");
				    }
				    if ($k == 'id') {
				    	$output .= '"'.$data.'"';
				    } else {
				    	$output .= ';"'.$data.'"';
				    }
				}
				$output .= "\n";
			}
	
			echo $output;
			exit;
		} else {
			return '';
		}

	}


}

?>