<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
* Class ModuleSeminareventsList
*
* Front end module "seminar list".
* @copyright  Gerd Regnery 2013
* @author     Gerd Regnery <http://www.webdesign-impulse.de>
* @package    Seminarverwaltung
*/
class ModuleSeminareventsList extends SeminarEvents
{

	/**
	* Current date object
	* @var integer
	*/
	protected $Date;

	/**
	* Template
	* @var string
	*/
	protected $strTemplate = 'mod_seminarevents_list';
	
	/**
	* Current URL
	* @var string
	*/
	protected $strUrl;

	/**
	* Display a wildcard in the back end
	* @return string
	*/
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### SEMINAR EVENTS LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		return parent::generate();
	}

	/**
	* Generate module
	*/
	protected function compile()
	{
		global $objPage;
		//
		// Get Seminar Categories
		//
 		$time = time();
 		$arrDebug = array();
		$strUrl = $this->strUrl;

		$blnClearInput = false;

	    $seminaridx = $this->Input->get('seminar');
	    if (empty($seminaridx)) {
		    $seminaridx = $this->Session->get('seminaridx');
			if (empty($seminaridx)) {
		    	$seminaridx = $this->Session->get('seminarid');
		    }
	    }
		// Sprung zum Buchenformular ermitteln aus Modul
		$sqlBookPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$bookJumpTo = $this->Database->prepare($sqlBookPg)
          	->execute($this->sv_jumpToBuchen);
		if ($bookJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($bookJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpToBuchen;
		}
		$jumpToBuchen = $tarPage;
		
		// Sprung zum Seminarreader ermitteln aus Modul
		$sqlModPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$modJumpTo = $this->Database->prepare($sqlModPg)
          	->execute($this->sv_jumpTo);
		if ($modJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($modJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpTo;
		}
		$modJumpTo = $tarPage;
		//
		// Seminar ermitteln
		//
		$arrSeminar = array();
		$arrEventList = array();
		$sqlSem = "SELECT * FROM tl_seminar WHERE id=?".
				  (!BE_USER_LOGGED_IN ? "  AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
		$objSem = $this->Database->prepare($sqlSem)->execute($seminaridx);
		if ($objSem->numRows) {
			$semId     = $objSem->id;
			$semTitle  = $objSem->title;
			$semAlias  = $objSem->alias;
			$semJumpTo = $objSem->sv_jumpTo;
			$semSort   = ($objSem->sortindex ? $objSem->sortindex : $semKey);
			$semKey++;
			//
			// wenn Modul jumpTo nicht gesetzt, dann Kategorie jumpTo nutzen
			//
			if (empty($this->sv_jumpTo)) {
				// Sprung zum Seminarreader ermitteln
				$sqlPg = "SELECT id, alias FROM tl_page WHERE id=?";
				$objPg = $this->Database->prepare($sqlPg)
		          	->execute($semJumpTo);
				if ($objPg->numRows) {
					$tarPage = $this->generateFrontendUrl($objPg->row(),$strUrl);
				} else {
					$tarPage = $semJumpTo;
				}
				$jt = $tarPage;
			} else {
				$jt = $modJumpTo;
			}
			$objCat = $this->Database->prepare("SELECT id,title,alias FROM tl_seminar_category WHERE id=?")
							->limit(1)->execute($objSem->pid);
			$category = '';
			$categoryId = 0;
			if ($objCat->numRows) {
				$category = $objCat->title;
				$categoryId = $objCat->id;
			}				
			$arrSeminar        	   = $objSem->row();
			$arrSeminar['category'] = $category;
			$arrSeminar['categoryid'] = $categoryId;
			// Start GR 19.04.2014
			//
			if (!empty($arrSeminar['singleSRC'])) {
				$objFile = \FilesModel::findByUuid($arrSeminar['singleSRC']);
				if (!empty($objFile)){
					if (is_file(TL_ROOT . '/' . $objFile->path)) {
						$arrSeminar['src'] = $objFile->path;
					}
				}
			}
			$arrSeminar['imageUrl'] = $arrSeminar['src'];
			// get Referent
			// Start GR 30.04.2014 
			$arrReferentData = $this->getReferentData($arrSeminar['facilitator']);
			$arrSeminar['referent'] = $arrReferentData['referent'];
			$arrSeminar['arrReferent'] = $arrReferentData['arrReferent'];
			// Zielseite vom ReaderModul aus ermitteln
			// $sql = "SELECT id,alias FROM tl_page WHERE id=(SELECT pid FROM tl_article WHERE id=(SELECT pid FROM `tl_content` WHERE module=?))";
			$sql0 = "SELECT pid FROM tl_content WHERE module=?";
			$objC = $this->Database->prepare($sql0)->execute($this->sv_cal_readerModule);
			if ($objC->numRows) {
				$cpid = $objC->pid;
				$sql1 = "SELECT pid FROM tl_article WHERE id=?";
				$objA = $this->Database->prepare($sql1)->execute($cpid);
				if ($objA->numRows) {
					$apid = $objA->pid;
					$sql = "SELECT id,alias FROM tl_page WHERE id=?";
					$objPg = $this->Database->prepare($sql)->execute($apid);
					if ($objPg->numRows) {
						$href = $this->generateFrontendUrl($objPg->row());
					}
				}
			}
			$arrDataRef = $this->getReferencesData($this,$categoryId,$arrSeminar['id'],0);
			// start 27.02.2015 GR StartZeit mitgeben
			if (empty($href)) {
				$href = $arrDataRef['href'].'&intStart='.$arrEvent['startTime'].'&intEnd='.$arrEvent['endTime'];
			} else {
				$href = ($arrDataRef['href'] ? $arrDataRef['href'] : $arrDataRef['href_booking']).'&intStart='.$arrEvent['startTime'].'&intEnd='.$arrEvent['endTime'];
			}
			// ende 27.02.2015 GR StartZeit mitgeben
			$arrSeminar['href'] = $href;
			$arrSeminar['href_seminar'] = $href;
			// Ende GR 30.04.2014 
			
			//
			// Event Liste
			// von heute bis max. 1 Jahr

			// Jump to the current period
			if (!isset($_GET['year']) && !isset($_GET['month']) && !isset($_GET['day']))
			{
				switch ($this->cal_format)
				{
					case 'cal_year':
						$this->Input->setGet('year', date('Y'));
						break;
	
					case 'cal_month':
						$this->Input->setGet('month', date('Ym'));
						break;
	
					case 'cal_day':
						$this->Input->setGet('day', date('Ymd'));
						break;
				}
	
				$blnClearInput = true;
			}
	
			$blnDynamicFormat = (!$this->cal_ignoreDynamic && in_array($this->cal_format, array('cal_day', 'cal_month', 'cal_year')));
	
			// Display year
			if ($blnDynamicFormat && $this->Input->get('year'))
			{
				$this->Date = new Date($this->Input->get('year'), 'Y');
				$this->cal_format = 'cal_year';
				$this->headline .= ' ' . date('Y', $this->Date->tstamp);
			}
	
			// Display month
			elseif ($blnDynamicFormat && $this->Input->get('month'))
			{
				$this->Date = new Date($this->Input->get('month'), 'Ym');
				$this->cal_format = 'cal_month';
				$this->headline .= ' ' . $this->parseDate('F Y', $this->Date->tstamp);
			}
	
			// Display day
			elseif ($blnDynamicFormat && $this->Input->get('day'))
			{
				$this->Date = new Date($this->Input->get('day'), 'Ymd');
				$this->cal_format = 'cal_day';
				$this->headline .= ' ' . $this->parseDate($objPage->dateFormat, $this->Date->tstamp);
			}
	
			// Display all events or upcoming/past events
			else
			{
				$this->Date = new Date();
			}
			list($strBegin, $strEnd, $strEmpty) = $this->getDatesFromFormat($this->Date, $this->cal_format);
			$strBegin = time();
			$strEnd   = (time()+365*24*60*60);
			$arrDebug[] = 'Id '. $semId.' begin '.$strBegin.' end '.$strEnd.' tm '.time();
			$arrEventList = $this->getAllSeminarEvents($semId,$strBegin,$strEnd);
			$arrDebug[] = $arrEventList;
		}
		
		// Subtemplate aufbereiten
		$strSeminar = '';
		$objTemplate = new FrontendTemplate($this->sv_seminar_template);
		$objTemplate->setData($arrSeminar);
		$strSeminar .= $objTemplate->parse();
		//
		$strEvents = '';

		$arrEvents = array();
		$dateBegin = date('Ymd', $strBegin);
		$dateEnd = date('Ymd', $strEnd);

		// Remove events outside the scope
		foreach ($arrEventList as $key=>$days)
		{
			if ($key < $dateBegin || $key > $dateEnd)
			{
				continue;
			}

			foreach ($days as $day=>$events)
			{
				foreach ($events as $event)
				{
					$event['firstDay'] = $GLOBALS['TL_LANG']['DAYS'][date('w', $day)];
					$event['firstDate'] = $this->parseDate($objPage->dateFormat, $day);
					$event['datetime'] = date('Y-m-d', $day);
					$arrEvents[] = $event;
				}
			}
		}

		unset($arrEventList);

		foreach($arrEvents as $event) {
			$objTemplate = new FrontendTemplate($this->sv_seminarevent_template);
			$objTemplate->setData($event);
			$strEvents .= $objTemplate->parse();
		} 
		// Pagination, sofern vorgesehen
		$i=0;
		// get date for page pagination
		$perPage = $this->perPage;
		$total = count($arrEventList);
		$limit = $total;
		$offset = 0;
		// Pagination
		if ($perPage > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			$offset = ($page - 1) * $perPage;
			$limit = min($perPage + $offset, $total);

			$objPagination = new Pagination($total, $perPage);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}
		$j = 0;
		unset($arrSeminarsPage);
		for ($i=$offset; $i<$limit; $i++)	{
			$arrSeminarsPage[$j] = $arrEventList[$i];
			$j++;
		}
		// Daten an SubTemplate $this->categories übergeben
		$this->Template->category = $category;
		$this->Template->categoryid = $categoryId;
		$this->Template->seminar = $strSeminar;
		$this->Template->events  = $strEvents;
		$this->Template->debug   = $arrDebug;

		if ($blnClearInput)
		{
			$this->Input->setGet('year', null);
			$this->Input->setGet('month', null);
			$this->Input->setGet('day', null);
		}

	}
}

?>