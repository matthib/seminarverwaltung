<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
* Class ModuleSeminarList
*
* Front end module "seminar list".
* @copyright  Gerd Regnery 2013
* @author     Gerd Regnery <http://www.webdesign-impulse.de>
* @package    Seminarverwaltung
*/
class ModuleSeminarList extends SeminarEvents
{

	/**
	* Current date object
	* @var integer
	*/
	protected $Date;

	/**
	* Template
	* @var string
	*/
	protected $strTemplate = 'mod_seminar';
	
	/**
	* Current URL
	* @var string
	*/
	protected $strUrl;

	/**
	* Display a wildcard in the back end
	* @return string
	*/
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### SEMINAR LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		return parent::generate();
	}

	/**
	* Generate module
	*/
	protected function compile()
	{
		global $objPage;
		//
		// Get Seminar Categories
		//
 		$time = time();

		$strUrl = $this->strUrl;

		// Sprung zum Buchenformular ermitteln aus Modul
		$sqlBookPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$bookJumpTo = $this->Database->prepare($sqlBookPg)
          	->execute($this->sv_jumpToBuchen);
		if ($bookJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($bookJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpToBuchen;
		}
		$jumpToBuchen = $tarPage;
		
		// Sprung zum Seminarreader ermitteln aus Modul
		$sqlModPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$modJumpTo = $this->Database->prepare($sqlModPg)
          	->execute($this->sv_jumpTo);
		if ($modJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($modJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpTo;
		}
		$modJumpTo = $tarPage;
		//
		// Sortkey wenn kein sortindex gesetzt ist
		//
		$catKey = 0;
		//
		// Kategorien aus Modulliste auslesen
		// 
		$arrCategoryList = array();
		$arrCategories = deserialize($this->sv_seminar_category);
		foreach($arrCategories as $catid) {
			$sqlCat = "SELECT * FROM tl_seminar_category WHERE id=?"; //.
//					  (!BE_USER_LOGGED_IN ? "  AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objCat = $this->Database->prepare($sqlCat)->execute($catid);
			if ($objCat->numRows) {
				$catId     = $objCat->id;
				$catTitle  = $objCat->title;
				$catAlias  = $objCat->alias;
				$catJumpTo = $objCat->sv_jumpTo;
				$catSort   = ($objCat->sortindex ? $objCat->sortindex : $catKey);
				$catKey++;
				//
				// wenn Modul jumpTo nicht gesetzt, dann Kategorie jumpTo nutzen
				//
				$arrCategory = $this->getReferencesData($this,$catId);
				$arrCategory['id'] 	   = $catId;
				$arrCategory['title']  = $catTitle;
				$arrCategory['alias']  = $catAlias;
				$arrCategory['href']   = $jt;
				$arrCategoryList[$catSort] = $arrCategory;
			}
		}
		// sortiere Kategorien
		if (!empty($arrCategoryList)) {
			ksort($arrCategoryList);
		}
		//
		// Seminare ermitteln
		//
		$semKey = 0;
		//
		$arrSeminarList = array();
		foreach ($arrCategoryList as $arrCatData) {
			$pid 		= $arrCatData['id'];
			$href		= $arrCatData['href'];
			$category 	= $arrCatData['title']; 
			$sqlSem = "SELECT * FROM tl_seminar WHERE pid=?".
					  (!BE_USER_LOGGED_IN ? "  AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objSem = $this->Database->prepare($sqlSem)->execute($catid);
			while ($objSem->next()) {
				$semId     = $objSem->id;
				$semTitle  = $objSem->title;
				$semAlias  = $objSem->alias;
				$semJumpTo = $objSem->sv_jumpTo;
				$semSort   = ($objSem->sortindex ? $objSem->sortindex : $semKey);
				$semKey++;
				//
				// wenn Modul jumpTo nicht gesetzt, dann Kategorie jumpTo nutzen
				//
/*
				if (empty($this->sv_jumpTo) && empty($href)) {
					// Sprung zum Seminarreader ermitteln
					$sqlPg = "SELECT id, alias FROM tl_page WHERE id=?";
					$objPg = $this->Database->prepare($sqlPg)
			          	->execute($semJumpTo);
					if ($objPg->numRows) {
						$tarPage = $this->generateFrontendUrl($objPg->row(),$strUrl);
					} else {
						$tarPage = $semJumpTo;
					}
					$jt = $tarPage;
				} else {
					$jt = ($href ? $href : $modJumpTo);
				}
*/
				$arrSeminar        	   = $objSem->row();
				$arrSeminar['category'] = $category;
				$arrSeminar['categoryid'] = $pid;
				$arrDataRef = $this->getReferencesData($this,$pid,$semId);
				$arrSeminar = array_merge($arrSeminar,$arrDataRef);
				$arrDataReferent = $this->getReferentData($objSem->facilitator);
				$arrSeminar['referent'] = $arrDataReferent['referent'];
				$arrSeminar['arrReferent'] = $arrDataReferent['arrReferent'];
				//$arrSeminar['href']    = $jt.'?seminaridx='.$semId;
				$arrSeminarList[$semSort] = $arrSeminar;
			}
		}
		// sortiere Kategorien
		if (!empty($arrSeminarList)) {
			ksort($arrSeminarList);
		}
		// Subtemplate aufbereiten
		$strSeminars = '';
		foreach($arrSeminarList as $seminar) {
			$objTemplate = new FrontendTemplate($this->sv_seminar_template);
			$objTemplate->setData($seminar);
			$strSeminars .= $objTemplate->parse();
		} 
		// Pagination, sofern vorgesehen
		$i=0;
		// get date for page pagination
		$perPage = $this->perPage;
		$total = count($arrSeminarList);
		$limit = $total;
		$offset = 0;
		// Pagination
		if ($perPage > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			$offset = ($page - 1) * $perPage;
			$limit = min($perPage + $offset, $total);

			$objPagination = new Pagination($total, $perPage);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}
		$j = 0;
		unset($arrSeminarsPage);
		for ($i=$offset; $i<$limit; $i++)	{
			$arrSeminarsPage[$j] = $arrSeminarList[$i];
			$j++;
		}
		// Daten an SubTemplate $this->categories übergeben
		$this->Template->seminars = $strSeminars;
		$this->Template->debug = $arrCategoryList;

	}
}

?>