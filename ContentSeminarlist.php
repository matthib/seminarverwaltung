<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

define('SV_SORT_NONE','sv_sort_none');
define('SV_SORT_DATE_ASC','sv_sort_date_asc');
define('SV_SORT_DATE_DESC','sv_sort_date_desc');
define('SV_SORT_ALPHA_ASC','sv_sort_alpha_asc');
define('SV_SORT_ALPHA_DESC','sv_sort_alpha_desc');

/**
* Class ContentSeminarlist
*
* Front end module "seminar list".
* @copyright  Gerd Regnery 2011 - 2015
* @author     Gerd Regnery <http://www.webdesign-impulse.de>
* @package    Seminarverwaltung
*/
class ContentSeminarlist extends SeminarManager
{
	/**
	* Current date object
	* @var integer
	*/
	protected $Date;

	/**
	* Template
	* @var string
	*/
	protected $strTemplate = 'mod_seminarlist';
	
	/**
	* Current URL
	* @var string
	*/
	protected $strUrl;


	/**
	* Display a wildcard in the back end
	* @return string
	*/
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### CONTENT ELEMENT SEMINAR LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		return parent::generate();
	}


	/**
	* Generate module
	*/
	protected function compile()
	{
		// Start GR 07.03.2015
		global $objPage;
		$blnClearInput = false;
 
 		// Jump to the current period
		if (!isset($_GET['year']) && !isset($_GET['month']) && !isset($_GET['day']))
		{
			switch ($this->svcal_format)
			{
				case 'sv_year':
					$this->Input->setGet('year', date('Y'));
					break;

				case 'sv_month':
					$this->Input->setGet('month', date('Ym'));
					break;

				case 'sv_day':
					$this->Input->setGet('day', date('Ymd'));
					break;
			}

			$blnClearInput = true;
		}

		$blnDynamicFormat = (!$this->svcal_ignoreDynamic && in_array($this->svcal_format, array('sv_day', 'sv_month', 'sv_year')));
		// Display year
		if ($blnDynamicFormat && $this->Input->get('year'))
		{
			$this->Date = new Date($this->Input->get('year'), 'Y');
			$this->svcal_format = 'sv_year';
			$this->headline .= ' ' . date('Y', $this->Date->tstamp);
		}

		// Display month
		elseif ($blnDynamicFormat && $this->Input->get('month'))
		{
			$this->Date = new Date($this->Input->get('month'), 'Ym');
			$this->svcal_format = 'sv_month';
			$this->headline .= ' ' . $this->parseDate('F Y', $this->Date->tstamp);
		}

		// Display day
		elseif ($blnDynamicFormat && $this->Input->get('day'))
		{
			$this->Date = new Date($this->Input->get('day'), 'Ymd');
			$this->svcal_format = 'sv_day';
			$this->headline .= ' ' . $this->parseDate($objPage->dateFormat, $this->Date->tstamp);
		}

		// Display all events or upcoming/past events
		else
		{
			$this->Date = new Date();
			$this->headline .= ' ' . $this->parseDate('F Y', $this->Date->tstamp);
		}

		list($strBegin, $strEnd, $strEmpty) = $this->getDatesFromFormat($this->Date, $this->svcal_format);
		// Ende GR 07.03.2015

		//
		// Get Seminar Categories
		//
		$arrSeminars = array();
		$i = 0;
 		$time = time();
 		// Debug output
 		$arrDebug = array();
 		//
		$strUrl = $this->strUrl;
		$jumpTo = $strUrl;
		$sort = SV_SORT_NONE;
		if ($this->seminar_sort) {
			$sort = $this->seminar_sort;
		}
		$counter = 0;
		$key = 0;
		//
		$arrSeminars = array();
		$arrCategories = deserialize($this->seminar_category);
		$this->Template->arrCat = $arrCategories;
		foreach($arrCategories as $catid) {
			$this->Template->catid = $catid;
			$objCat = $this->Database->prepare("SELECT id,alias,title,sv_jumpTo FROM tl_seminar_category WHERE id=?")
			->execute($catid);
			if ($objCat->numRows) {
				$category = $objCat->title;
				$catId    = $objCat->id;
		    } else {
				continue;
			}
			//
			// Get Category jumpTo
			//
			$catJumpTo = $this->getReaderPage($catid,'tl_seminar_category','sv_jumpTo');
			//
			// Get all seminars oder by creation
			//
			$j = 0;
			$sql = "SELECT * FROM tl_seminar WHERE pid=? ".(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$result = $this->Database->prepare($sql)->execute($catId);
			if ($result->numRows) {
				while ($result->next()) {
					//$arrDebug[] = '   '.$result->title;
					$j++;
					$arrSeminar = Array();
					$arrSeminar = $result->row();
					$titel = str_replace(array(' ','+','-','\\','/'),array('_','_PLUS_','_MINUS_','_BS_','_SLASH_'),trim($result->title));
					//
					// Get Seminar jumpTo
					//
					if ($result->source != 'default') {
						$semJumpTo = $this->getReaderPage($result->id,'tl_seminar','sv_jumpTo');
					} else {
						$semJumpTo = '';
					}
					//
					// Get Booking jumpToBooking reference
					//
					$bookJumpTo = $this->getReaderPage($this->sv_jumpToBooking,NULL,NULL);
					if (empty($bookJumpTo)) {
						$bookJumpTo = $this->getReaderPage($result->id,'tl_seminar','jumpToBooking');
					}
					// Module/ContentElement jumpTo
					$cteJumpTo = $this->getReaderPage($this->sv_jumpTo,NULL,NULL);
					// get last jumpTo od hierarchie
					if (!empty($cteJumpTo)) {
						$jumpTo = $cteJumpTo;
					} else if (!empty($semJumpTo)) {
					    $jumpTo = $semJumpTo;
					} else if (!empty($catJumpTo)) {
						$jumpTo = $catJumpTo;
					} else {
						$jumpTo = $this->Environment->requestUri;
					}
					//
					// Seminardaten ergänzen: href, referent, category
					//
					if (!empty($jumpTo)) {
						$arrSeminar['href'] = $jumpTo.'?seminar='.$result->id;
					} else {
						$arrSeminar['href'] = '';
					}
					if (!empty($bookJumpTo)) {
						$arrSeminar['href_booking'] = $bookJumpTo.'?seminar='.$result->id;
					} else {
						$arrSeminar['href_booking'] = '';
					}
					if (!empty($semJumpTo)) {
						$arrSeminar['href_seminar'] = $semJumpTo.'?seminar='.$result->id;
					} else {
						$arrSeminar['href_seminar'] = '';
					}
					if (!empty($catJumpTo)) {
						$arrSeminar['href_category'] = $catJumpTo.'?seminar='.$result->id;
					} else {
						$arrSeminar['href_category'] = '';
					}

					//
					// Start GR 14.03.2014
					//
					if (!empty($result->singleSRC)) {
						$objFile = \FilesModel::findByUuid($result->singleSRC);
						if (!empty($objFile)){
							if (is_file(TL_ROOT . '/' . $objFile->path)) {
								$arrSeminar['src'] = $objFile->path;
							}
						}
					}
					$arrSeminar['category'] = $category;
					//
					// Start GR 11.02.2015 starTime und endTime eingesetzt damit auch die Tagesaktuellen Events angezeigt werden
					//
					$sql = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((startTime>=$time) OR (startTime<=$time AND endTime>=$time) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time) AND startTime<=$time))" . 
							(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . 
							" ORDER BY date";
					
					$datum = '00000000_0000';
					$objEvent = $this->Database->prepare($sql)
					->execute($result->id,$time,$time);
					if ($result->showalways && !$objEvent->numRows) {
						unset($arrEvents);
						$arrEvents = array();
						unset($arrEvent);
						$arrEvent = array();
						
						$arrSeminars[$counter] = $arrSeminar;
						$counter++;
						$datum = '00000000_0000';
					} else {
						//
						// get startdate
						//
						$k = 0;
						// initial value for array index key
						unset($arrEvents);
						$arrEvents = array();
						unset($arrEvent);
						$arrEvent = array();
						$datum = '00000000_0000';
					    $blnFirst = true;
						while ($objEvent->next()) {
						    $k++;
							$arrEvent = $objEvent->row();
							$startDt = date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->date);
	    					$startTm = date($GLOBALS['TL_CONFIG']['timeFormat'],$objEvent->startTime);
	    					$endTm   = date($GLOBALS['TL_CONFIG']['timeFormat'],$objEvent->endTime);
	    					$start   = $objEvent->start ? date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->start) : '';
	    					$stop    = $objEvent->stop  ? date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->stop) : '';
	    					// EndDatum nur dann ausgeben, wenn es größer als das Startdatum ist
	    					
	    					if ($objEvent->endDate <= $objEvent->date) {
	    						$endDt = null;
	    					} else {
	    						$endDt   = date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->endDate);
	    					}
	    					$intern  = empty($objEvent->intern) ? trim($result->intern) : trim($objEvent->intern);
	    					$intern = str_replace(array(' ','+','/'),array('_','_PLUS_','_BS_'),$intern);
							$loc = $objEvent->location ? $objEvent->location : ($result->location ? $result->location : '' );
	    					$currentkey = date('Ymd_Hi',$objEvent->date).'_'.$loc.'_'.$intern;	
	    					if ($blnFirst) {
	    						$datum = $currentkey;
	    						$blnFirst = false;
	    				    }	
							$arrEvent = $objEvent->row();
							$arrEvent['category'] = $category;
							if (!empty($jumpTo)) {
								$arrEvent['href'] = $arrSeminar['href'].'&event='.$arrEvent['id'];
							} else {
								$arrEvent['href'] = $this->Environment->url;
							}	
							//
							if (!empty($semJumpTo)) {
								$arrEvent['href_event'] = $arrSeminar['href_seminar'].'&event='.$arrEvent['id'];
							} else {
								$arrEvent['href_event'] = '';
							}
							if (!empty($bookJumpTo)) {
								$arrEvent['href_booking'] = $arrSeminar['href_booking'].'&event='.$arrEvent['id'];
							} else {
								$arrEvent['href_booking'] = '';
							}
							//$arrEvent['buchen_formular'] = $jumpToBuchen.'?seminaridx='.$result->id.'&eventidx='.$objEvent->id;
							$arrEvent['startDate'] = $startDt;
							$arrEvent['startTime'] = $startTm;
							$arrEvent['endTime']   = $endTm;
							$arrEvent['endDate']   = $endDt;
							$arrEvent['startTime_unix'] = $objEvent->startTime;
							$arrEvent['endTime_unix']   = $objEvent->endTime;
							$arrEvent['startDate_Raw'] = $objEvent->date;
							$arrEvent['endTime_Raw']   = $objEvent->endDate;
							$arrEvent['startTime_Raw'] = $objEvent->startTime;
							$arrEvent['endTime_Raw']   = $objEvent->endTime;
							if (empty($endDt)) {
								$arrEvent['endDate_unix']   = NULL;
							} else {
								$arrEvent['endDate_unix']   = $objEvent->endDate;
							}
							//$arrEvent['intern']    = $intern;

							// Start GR 25.04.2014 
							$arrData = $this->getReferentData($objEvent->facilitator);
							$arrEvent['referent'] = $arrData['referent'];
							$arrEvent['arrReferent'] = $arrData['arrReferent'];
							//$arrDataBk = $this->getBookingState($result->places,$result->places_min,$objEvent->places_booked);
							//$arrEvent['bookstate_class'] = $arrDataBk['bookingstate'];
							//$arrEvent['free_places'] = $arrDataBk['free_places'];
							$arrDataBooking = $this->getBookingState($arrSeminar['places'],$arrSeminar['places_min'],$objEvent->places_booked);
							$arrDataICAL = $this->getICALEvent($arrEvent,$arrSeminar);	
							$arrEvent = array_merge($arrDataBooking,$arrEvent);
							$arrEvent = array_merge($arrDataICAL,$arrEvent);
							
							$GLOBALS['ICAL'][$objEvent->id] = $arrDataICAL;			

							// Ende GR 25.04.2014 

							//
							// Start GR 08.02.2014
							// Multi-day event als UnterArray speichern
							//
							$arrList = array();
							if ($objEvent->recurring) {
								$arrList = $this->getRecurringEvents($objEvent,$strBegin,$strEnd,$arrEvent);
								$arrEvent['arrRecurring'] = $arrList;
								$arrNext = current($arrList);
								$arrSeminar['nextDate'] = $arrNext['date'];
								$arrSeminar['nextDateRaw'] = $arrNext['dateRaw'];
								$arrSeminar['nextDateCount'] = $arrNext['count'];
							}	
							//
							// Ende GR 08.02.2014
							//
							$arrEvents[$currentkey]    = $arrEvent;
							//
							// Start GR 06.03.2015 Wiederholungstermine in die Liste holen
							//
							if ($this->sv_show_recurring && !empty($arrList)) {
								foreach ($arrList as $arrEvtR) {
									$currentkeyR = date('Ymd_Hi',$arrEvtR['startTime_Raw']).'_'.$loc.'_'.$intern;	
									$arrEvents[$currentkeyR] = $arrEvtR;
								}
							}
							//
							// Ende GR 06.03.2015 Wiederholungstermine in die Liste holen
							//
							// next item
							$i++;
						}
						ksort($arrEvents);
						$dateBegin = date('Ymd', $strBegin);
						$dateEnd = date('Ymd', $strEnd);
						foreach ($arrEvents as $nr=>$event) {
							$compare = date("Ymd",$event['startDate_Raw']);
							if ($compare >= $dateBegin && $compare <= $dateEnd) {
		    					$currentkey = date('Ymd_Hi',$event['date']).'_'.$loc.'_'.$intern;
	    						break;	
							}
						}
						$datum = $currentkey;
						if ($this->svcal_order == 'ascending') {
							ksort($arrEvents);
						} else {
							krsort($arrEvents);
						}
						$arrDates = array();
						foreach($arrEvents as $arrEvent) {
							$arrDates[] = $arrEvent; 
						}
						$arrSeminar['arrEvents'] = $arrDates;
						// Start GR 25.04.2014 
						$arrData = $this->getReferentData($result->facilitator);
						$arrSeminar['facilitatorname'] = $arrData['referent']; // bleibt aus Kompatibilitätsgründen
						$arrSeminar['referent'] = $arrData['referent'];
						$arrSeminar['arrReferent'] = $arrData['arrReferent'];
						// Ende GR 25.04.2014 
						
						$sortKey = $this->getSortKey($sort,$counter,$datum,$titel);
						$arrSeminar['sort'] = $sort;
						$arrSeminar['sortKey'] = $sortKey;
						
						$arrSeminars[$sortKey] = $arrSeminar;
						$counter++;
					}
				}
			}

		}
		// sort seminar list		
		if (!empty($arrSeminars)) {
		    if (($sort === SV_SORT_ALPHA_DESC) || ($sort === SV_SORT_DATE_DESC)) {
				krsort($arrSeminars);
		    } else {
				ksort($arrSeminars);
			}
		}
		
		// Start GR 07.03.2015
		$arrEvents = array();
		$dateBegin = date('Ymd', $strBegin);
		$dateEnd = date('Ymd', $strEnd);

		$arrSList = array();
		// Remove events outside the scope
		foreach ($arrSeminars as $key=>$seminars) {
			$arrEvents = array();
			foreach ($seminars['arrEvents'] as $nr=>$event) {
				$compare = date("Ymd",$event['startDate_Raw']);
				if ($compare < $dateBegin || $compare > $dateEnd) {
					continue;
				}
				$event['firstDay'] = $GLOBALS['TL_LANG']['DAYS'][date('w', $event['startDate_Raw'])];
				$event['firstDate'] = $this->parseDate($objPage->dateFormat, $event['startDate_Raw']);
				$event['datetime'] = date('Y-m-d', $event['startDate_Raw']);
				$arrEvents[] = $event;
			}
			$seminars['arrEvents'] = $arrEvents;
			$arrSList[] = $seminars;
		}
		// Ende GR 07.03.2015
		
		
		// get simple and sorted list for output
		unset($arrSeminar);
		foreach($arrSList as $arrSeminar) {    // GR 07.03.2015
			$arrSeminarList[] = $arrSeminar;
		}

		// do the pagination
		$i=0;
		// get date for page pagination
		$perPage = $this->seminar_perPage;
		$total = count($arrSeminarList);
		$limit = $total;
		$offset = 0;

		// Start GR 07.03.2015
		// Overall limit
		if ($this->seminar_limit > 0)
		{
			$total = min($this->seminar_limit, $total);
			$limit = $total;
		}
		// Ende GR 07.03.2015

		// Pagination
		if ($perPage > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			// Start GR 07.03.2015
			// Do not index or cache the page if the page number is outside the range
			if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
			{
				global $objPage;
				$objPage->noSearch = 1;
				$objPage->cache = 0;

				// Send a 404 header
				header('HTTP/1.1 404 Not Found');
				return;
			}
			// Ende GR 07.03.2015

			$offset = ($page - 1) * $perPage;
			$limit = min($perPage + $offset, $total);

			$objPagination = new Pagination($total, $perPage);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}
		$j = 0;
		unset($arrSeminarsPage);
		for ($i=$offset; $i<$limit; $i++)	{
			$arrSeminarsPage[$j] =	$arrSeminarList[$i];
			$j++;
		}
		// put all to templates
		$objTemplate = new FrontendTemplate($this->seminar_template);
		$this->Template->debug = $arrDebug;
		$objTemplate->arrSeminars = $arrSeminarsPage;
		$this->Template->seminars = $objTemplate->parse();
	}

	private function getRecurringEvents($objEvent, $intStart, $intEnd, $arrEvent) {
		global $objPage;
		$arrRecurring = array();
		// Recurring events
		if ($objEvent->recurring) {
			$count = 0;
			$arrRepeat = deserialize($objEvent->repeatEach);
			$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];
			if ($arrRepeat['value'] >= 1) {
				if ($objEvent->recurrences > 0) {
					while ($objEvent->endTime < $intEnd) {
						$count++;
						if ($count >= $objEvent->recurrences) {
							break;
						}
		
						$objEvent->startTime = strtotime($strtotime, $objEvent->startTime);
						$objEvent->endTime = strtotime($strtotime, $objEvent->endTime);
		
						// Skip events outside the scope
						if ($objEvent->endTime < $intStart || $objEvent->startTime > $intEnd) {
							continue;
						}
						
						//
						// Start GR 06.03.2015 Weiderholungsdatum als komplettes Datum anlegen
						//
						//$strRecurring = $this->parseDate($objPage->dateFormat, $objEvent->startTime);
						//$strRecurringEnd = $this->parseDate($objPage->dateFormat, $objEvent->endTime);
						$arrRecurring[$count-1] = $arrEvent;
						$startDt = date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->startTime);
    					$startTm = date($GLOBALS['TL_CONFIG']['timeFormat'],$objEvent->startTime);
    					$endTm   = date($GLOBALS['TL_CONFIG']['timeFormat'],$objEvent->endTime);
    					// EndDatum nur dann ausgeben, wenn es größer als das Startdatum ist
    					if ($objEvent->endDate <= $objEvent->date) {
    						$endDt = null;
    					} else {
    						$endDt   = date($GLOBALS['TL_CONFIG']['dateFormat'],$objEvent->endTime);
    					}
						$arrRecurring[$count-1]['date'] = $objEvent->startTime;
						$arrRecurring[$count-1]['startDate'] = $startDt;
						$arrRecurring[$count-1]['startDate_Raw'] = $objEvent->startTime;
						$arrRecurring[$count-1]['startTime'] = $startTm;
						$arrRecurring[$count-1]['startTime_Raw'] = $objEvent->startTime;
						$arrRecurring[$count-1]['startTime_unix'] = $objEvent->startTime;
						$arrRecurring[$count-1]['endDate'] = $endDt;
						$arrRecurring[$count-1]['endDate_Raw'] = $objEvent->endTime;
						$arrRecurring[$count-1]['endTime'] = $endTm;
						$arrRecurring[$count-1]['endTime_Raw'] = $objEvent->endTime;
						$arrRecurring[$count-1]['endTime_unix'] = $objEvent->endTime;
						if (empty($endDt)) {
							$arrEvent['endDate_unix']   = NULL;
						} else {
							$arrEvent['endDate_unix']   = $objEvent->endTime;
						}
						//
						// Ende GR 06.03.2015 Weiderholungsdatum als komplettes Datum anlegen
						//
						$arrRecurring[$count-1]['count'] = $count-1;
					}
				}
			}
		}
		return $arrRecurring;
	}


	//
	// getSortKey - Sortierschlüssel für das Array zusammensetzen
	//
	private function getSortKey($sort, $cnt, $date, $title) {
		if ($sort === SV_SORT_NONE) {
			$sortkey = $cnt.'_'.$date.'_'.$title;
		} else if ($sort === SV_SORT_DATE_ASC) {
			$sortkey = $date.'_'.$title.'_'.$cnt;
		} else if ($sort === SV_SORT_DATE_DESC) {
			$sortkey = $date.'_'.$title.'_'.$cnt;
		} else if ($sort === SV_SORT_ALPHA_ASC) {
			$sortkey = $title.'_'.$date.'_'.$cnt;
		} else if ($sort === SV_SORT_ALPHA_DESC) {
			$sortkey = $title.'_'.$date.'_'.$cnt;
		} else {
			$sortkey = $cnt;
		}
		return $sortkey;
	}
	//
	// getReaderPage - URL zur Zielseite ermitteln (indirekt und direkt)
	// .. direkt erfolgt wenn $strTable leer ist
	//
/*
	private function getReaderPage($id,$strTable,$strJumpTo) {
		// return default is empty
		$jt = '';
		// Page Select sql
	    $sql = "SELECT id, alias FROM tl_page WHERE id=?";
		// direct Page Selection
		if (empty($strTable)) {
		    $objJump = $this->Database->prepare($sql)
			    	->execute($id);
		    if ($objJump->numRows) {
		    	$jt = $this->generateFrontendUrl($objJump->row());
		    }
		// get jumpTo column first
		} else {
			$objTable = $this->Database->prepare("SELECT * FROM ".$strTable." WHERE id=?")
			->execute($id);
			if ($objTable->numRows) {
				$tarPage   = $objTable->$strJumpTo;
				//  Page Selection
			    $objJump = $this->Database->prepare($sql)
				    	->execute($tarPage);
			    if ($objJump->numRows) {
			    	$jt = $this->generateFrontendUrl($objJump->row());
			    }
			}
		}
		return $jt;
	}
*/
	//
	// getSeminarListSorted - Sortierte Liste der Seminare ASCending ermitteln
	//
	private function getSeminarListSorted() {
		$arrData = array();
		$sqlCat = "SELECT id,title FROM tl_seminar_category WHERE 1";
		$objCat = $this->Database->prepare($sqlCat)->execute();
		while ($objCat-next()) {
		    $pid = $objCat->id;
		    $title = $objCat->title;
		    $arrSeminar = array();
			$sqlSeminar = "SELECT title FROM tl_seminar WHERE pid=? AND published=1 ORDER BY id ASC";
			$objSeminar = $this->Database->prepare($sqlSeminar)->execute($pid);
			while ($objSeminar->next()) {
				$arrSeminar[]=$objSeminar->title;
			}
			$arrData[$title]=$arrSeminar;
		}
		return $arrData;
	}

	/**
	 * Calculate the span between two timestamps in days
	 * @param integer
	 * @param integer
	 * @return integer
	 */
	public static function calculateSpan($intStart, $intEnd)
	{
		return self::unixToJd($intEnd) - self::unixToJd($intStart);
	}


	/**
	 * Convert a UNIX timestamp to a Julian day
	 * @param integer
	 * @return integer
	 */
	public static function unixToJd($tstamp)
	{
		list($year, $month, $day) = explode(',', date('Y,m,d', $tstamp));

		// Make year a positive number
		$year += ($year < 0 ? 4801 : 4800);

		// Adjust the start of the year
		if ($month > 2)
		{
			$month -= 3;
		}
		else
		{
			$month += 9;
			--$year;
		}

		$sdn  = floor((floor($year / 100) * 146097) / 4);
		$sdn += floor((($year % 100) * 1461) / 4);
		$sdn += floor(($month * 153 + 2) / 5);
		$sdn += $day - 32045;

		return $sdn;
	}

}

?>