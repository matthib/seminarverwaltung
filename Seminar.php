<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/** TEST
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
 * Class Seminar
 *
 */
class Seminar extends Frontend
{

	/**
	 * Current events
	 * @var array
	 */
	protected $arrSeminars = array();

	/**
	 * Sort out protected archives
	 * @param array
	 * @return array
	 */
	protected function sortOutProtected($arrCategories)
	{
		if (BE_USER_LOGGED_IN || !is_array($arrCategories) || count($arrCategories) < 1)
		{
			return $arrCategories;
		}

		$this->import('FrontendUser', 'User');
		$objCategory = $this->Database->execute("SELECT id, protected, groups FROM tl_seminar_category WHERE id IN(" . implode(',', array_map('intval', $arrCategories)) . ")");
		$arrCategories = array();

		while ($objCategory->next())
		{
			if ($objCategory->protected)
			{
				if (!FE_USER_LOGGED_IN)
				{
					continue;
				}

				$groups = deserialize($objCategory->groups);

				if (!is_array($groups) || count($groups) < 1 || count(array_intersect($groups, $this->User->groups)) < 1)
				{
					continue;
				}
			}

			$arrCategories[] = $objCategory->id;
		}

		return $arrCategories;
	} 
	/**
	 * Update a particular RSS feed
	 * @param integer
	 */
	public function generateFeed($intId)
	{
		//$objCategory = $this->Database->prepare("SELECT * FROM tl_seminar WHERE id=? AND makeFeed=?")
		//							  ->limit(1)
		//							  ->execute($intId, 1);

		if ($objCategory->numRows < 1)
		{
			return;
		}

		$objCategory->feedName = strlen($objCategory->alias) ? $objCategory->alias : 'seminar' . $objCategory->id;

		// Delete XML file
		if ($this->Input->get('act') == 'delete' || $objCategory->protected)
		{
			$this->import('Files');
			$this->Files->delete($objCategory->feedName . '.xml');
		}

		// Update XML file
		else
		{
			$this->generateFiles($objCategory->row());
			$this->log('Generated event feed "' . $objCategory->feedName . '.xml"', 'Calendar generateFeed()', TL_CRON);
		}
	}


	/**
	 * Delete old files and generate all feeds
	 */
	public function generateFeeds()
	{
		$this->removeOldFeeds();
		$objCategory = $this->Database->execute("SELECT * FROM tl_seminar WHERE makeFeed=1 AND protected!=1");

		while ($objCategory->next())
		{
			$objCategory->feedName = strlen($objCategory->alias) ? $objCategory->alias : 'calendar' . $objCategory->id;

			$this->generateFiles($objCategory->row());
			$this->log('Generated event feed "' . $objCategory->feedName . '.xml"', 'Seminar generateFeeds()', TL_CRON);
		}
	}


	/**
	 * Generate an XML file and save it to the root directory
	 * @param array
	 */
	protected function generateFiles($arrArchive)
	{
		$time = time();
		$this->arrSeminars = array();
		$strType = ($arrArchive['format'] == 'atom') ? 'generateAtom' : 'generateRss';
		$strLink = strlen($arrArchive['feedBase']) ? $arrArchive['feedBase'] : $this->Environment->base;
		$strFile = $arrArchive['feedName'];

		$objFeed = new Feed($strFile);

		$objFeed->link = $strLink;
		$objFeed->title = $arrArchive['title'];
		$objFeed->description = $arrArchive['description'];
		$objFeed->language = $arrArchive['language'];
		$objFeed->published = $arrArchive['tstamp'];

		// Get upcoming events
		$objArticleStmt = $this->Database->prepare("SELECT *, (SELECT name FROM tl_user u WHERE u.id=e.author) AS authorName FROM tl_calendar_events e WHERE pid=? AND (startTime>=$time OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time))) AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1 ORDER BY startTime");

		if ($arrArchive['maxItems'] > 0)
		{
			$objArticleStmt->limit($arrArchive['maxItems']);
		}

		$objArticle = $objArticleStmt->execute($arrArchive['id']);

		// Get default URL
		$objParent = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
									->limit(1)
									->execute($arrArchive['jumpTo']);

		$strUrl = $strLink . $this->generateFrontendUrl($objParent->fetchAssoc(), '/events/%s');

		// Parse items
		while ($objArticle->next())
		{
			$this->addSeminar($objArticle, $objArticle->startTime, $objArticle->endTime, $strUrl, $strLink);

			// Recurring events
			if ($objArticle->recurring)
			{
				$count = 0;
				$arrRepeat = deserialize($objArticle->repeatEach);

				// Do not include more than 20 recurrences
				while ($count++ < 20)
				{
					if ($objArticle->recurrences > 0 && $count >= $objArticle->recurrences)
					{
						break;
					}

					$arg = $arrRepeat['value'];
					$unit = $arrRepeat['unit'];

					$strtotime = '+ ' . $arg . ' ' . $unit;

					$objArticle->startTime = strtotime($strtotime, $objArticle->startTime);
					$objArticle->endTime = strtotime($strtotime, $objArticle->endTime);

					if ($objArticle->startTime >= $time)
					{
						$this->addSeminar($objArticle, $objArticle->startTime, $objArticle->endTime, $strUrl, $strLink);
					}
				}
			}
		}

		$count = 0;
		ksort($this->arrSeminars);

		// Add feed items
		foreach ($this->arrSeminars as $days)
		{
			foreach ($days as $seminars)
			{
				foreach ($seminars as $seminar)
				{
					if ($arrArchive['maxItems'] > 0 && $count++ >= $arrArchive['maxItems'])
					{
						break(3);
					}

					$objItem = new FeedItem();

					$objItem->title = $seminar['title'];
					$objItem->link = $seminar['link'];
					$objItem->published = $seminar['published'];
					$objItem->start = $seminar['start'];
					$objItem->end = $seminar['end'];
					$objItem->author = $seminar['authorName'];

					// Prepare the description
					$strDescription = ($arrArchive['source'] == 'source_text') ? $seminar['description'] : $seminar['teaser'];
					$strDescription = $this->replaceInsertTags($strDescription);
					$objItem->description = $this->convertRelativeUrls($strDescription, $strLink);

					if (is_array($seminar['enclosure']))
					{
						foreach ($seminar['enclosure'] as $enclosure)
						{
							$objItem->addEnclosure($enclosure);
						}
					}

					$objFeed->addItem($objItem);
				}
			}
		}

		// Create file
		$objRss = new File($strFile . '.xml');
		$objRss->write($this->replaceInsertTags($objFeed->$strType()));
		$objRss->close();
	}


	/**
	 * Add events to the indexer
	 * @param array
	 * @param integer
	 * @param boolean
	 * @return array
	 */
	public function getSearchablePages($arrPages, $intRoot=0, $blnIsSitemap=false)
	{
		$arrRoot = array();

		if ($intRoot > 0)
		{
			$arrRoot = $this->getChildRecords($intRoot, 'tl_page');
		}

		$time = time();
		$arrProcessed = array();

		// Get all calendars
		$objCategory = $this->Database->prepare("SELECT id, sv_jumpTo FROM tl_seminar WHERE protected!=?")
									  ->execute(1);

		// Walk through each calendar
		while ($objCategory->next())
		{
			if (is_array($arrRoot) && count($arrRoot) > 0 && !in_array($objCategory->sv_jumpTo, $arrRoot))
			{
				continue;
			}

			// Get the URL of the jumpTo page
			if (!isset($arrProcessed[$objCategory->sv_jumpTo]))
			{
				$arrProcessed[$objCategory->sv_jumpTo] = false;

				// Get target page
				$objParent = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=? AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1 AND noSearch!=1" . ($blnIsSitemap ? " AND sitemap!='map_never'" : ""))
											->limit(1)
											->execute($objCategory->sv_jumpTo);
				// Determin domain
				if ($objParent->numRows)
				{
					$domain = $this->Environment->base;
					$objParent = $this->getPageDetails($objParent->id);

					$arrProcessed[$objCategory->sv_jumpTo] = $domain . $this->generateFrontendUrl($objParent->row(), ($GLOBALS['TL_CONFIG']['useAutoItem'] ?  '/%s' : '/seminars/%s'), $objParent->language);

				}
			}

			// Skip events without target page
			if ($arrProcessed[$objCategory->sv_jumpTo] === false)
			{
				continue;
			}

			$strUrl = $arrProcessed[$objCategory->sv_jumpTo];

			// Get items
			$objArticle = $this->Database->prepare("SELECT * FROM tl_seminar WHERE id=? AND source='default' AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1 ORDER BY start DESC")
										 ->execute($objCategory->id);

			// Add items to the indexer
			while ($objArticle->next())
			{
				$arrPages[] = sprintf($strUrl, (($objArticle->alias != '' && !$GLOBALS['TL_CONFIG']['disableAlias']) ? $objArticle->alias : $objArticle->id));
			}
		}

		return $arrPages;
	}

	/**
	 * Get all events of a certain period
	 * @param array
	 * @param integer
	 * @param integer
	 * @return array
	 */
	protected function getAllSeminars($category)
	{
		$this->import('String');

		$time = time();
		$this->arrSeminars = array();
		$id = $category;

			$strUrl = $this->strUrl;

			// Get current "jumpTo" page
			$objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=(SELECT sv_jumpTo FROM tl_seminar_category WHERE id=?)")
									  ->limit(1)
									  ->execute($id);

			if ($objPage->numRows)
			{
				$strUrl = $this->generateFrontendUrl($objPage->row(), ($GLOBALS['TL_CONFIG']['useAutoItem'] ? '/%s' : '/seminars/%s'));
			}

			// Get events of the current period
			$sql = "SELECT *, (SELECT title FROM tl_seminar_category WHERE id=?) AS category, (SELECT name FROM tl_user WHERE id=author) author FROM tl_seminars WHERE pid=?" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objSeminars = $this->Database->prepare($sql)
										->execute($id, $id);

			if ($objSeminars->numRows < 1)
			{
				continue;
			}

			while ($objSeminars->next())
			{
				$this->addSeminar($objSeminars, $strUrl, $id);
			}

		// Sort data
		foreach (array_keys($this->arrSeminars) as $key)
  	{
			ksort($this->arrSeminars[$key]);
		}

		// HOOK: modify result set
		if (isset($GLOBALS['TL_HOOKS']['getAllSeminars']) && is_array($GLOBALS['TL_HOOKS']['getAllSeminars']))
		{
			foreach ($GLOBALS['TL_HOOKS']['getAllSeminars'] as $callback)
			{
				$this->import($callback[0]);
				$this->arrSeminars = $this->$callback[0]->$callback[1]($this->arrSeminars, $arrCategories, $this);
			}
		}

		return $this->arrSeminars;
	} 

	/**
	 * Add an event to the array of active events
	 * @param object
	 * @param integer
	 * @param integer
	 * @param string
	 * @param string
	 */
	protected function addSeminar(Database_Result $objArticle, $strUrl, $strLink)
	{

		global $objPage;
		$this->import('String');

		// Add title and link
		$title = $objArticle->title;
		$link = '';
		switch ($objArticle->source)
		{
			case 'external':
				$link = $objArticle->url;
				break;

			case 'internal':
				$objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
									 	  ->limit(1)
										  ->execute($objArticle->sv_jumpTo);

				if ($objPage->numRows)
				{
					$link = $strLink . $this->generateFrontendUrl($objPage->row());
				}
				break;

			case 'article':
				$objPage = $this->Database->prepare("SELECT a.id AS aId, a.alias AS aAlias, a.title, p.id, p.alias FROM tl_article a, tl_page p WHERE a.pid=p.id AND a.id=?")
										  ->limit(1)
										  ->execute($objArticle->articleId);

				if ($objPage->numRows)
				{
					$link = $strLink . $this->generateFrontendUrl($objPage->row(), '/articles/' . ((!$GLOBALS['TL_CONFIG']['disableAlias'] && strlen($objPage->aAlias)) ? $objPage->aAlias : $objPage->aId));
				}
				break;
		}

		// Link to default page
		if ($link == '')
		{
			$link = sprintf($strUrl, ((strlen($objArticle->alias) && !$GLOBALS['TL_CONFIG']['disableAlias']) ? $objArticle->alias : $objArticle->id));
		}

		// Clean the RTE output
		if ($objPage->outputFormat == 'xhtml')
		{
			$objArticle->teaser = $this->String->toXhtml($objArticle->teaser);
		}
		else
		{
			$objArticle->teaser = $this->String->toHtml5($objArticle->teaser);
		}

		$arrSeminar = array
		(
			'title' => $title,
			'description' => $objArticle->details,
			'teaser' => $objArticle->teaser,
			'link' => $link,
			'published' => $objArticle->tstamp,
			'authorName' => $objArticle->authorName
		);

		// Enclosure
		if ($objArticle->addEnclosure)
		{
			$arrEnclosure = deserialize($objArticle->enclosure, true);

			if (is_array($arrEnclosure))
			{
				foreach ($arrEnclosure as $strEnclosure)
				{
					if (is_file(TL_ROOT . '/' . $strEnclosure))
					{				
						$arrSeminars['enclosure'][] = $strEnclosure;
					}
				}
			}
		}

		$this->arrSeminars[$objArticle->internal][] = $arrSeminar;
	}

	/**
	 * Calculate the span between two timestamps in days
	 * @param integer
	 * @param integer
	 * @return integer
	 */
	public static function calculateSpan($intStart, $intEnd)
	{
		return self::unixToJd($intEnd) - self::unixToJd($intStart);
	}


	/**
	 * Convert a UNIX timestamp to a Julian day
	 * @param integer
	 * @return integer
	 */
	public static function unixToJd($tstamp)
	{
		list($year, $month, $day) = explode(',', date('Y,m,d', $tstamp));

		// Make year a positive number
		$year += ($year < 0 ? 4801 : 4800);

		// Adjust the start of the year
		if ($month > 2)
		{
			$month -= 3;
		}
		else
		{
			$month += 9;
			--$year;
		}

		$sdn  = floor((floor($year / 100) * 146097) / 4);
		$sdn += floor((($year % 100) * 1461) / 4);
		$sdn += floor(($month * 153 + 2) / 5);
		$sdn += $day - 32045;

		return $sdn;
	}
}

?>