<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
 * Class ModuleFormularBuchung
 *
 * @copyright  Gerd Regnery 2011
 * @author     Gerd Regnery
 * @package    Seminarverwaltung
 */
define('SENDMAIL_OK', 0);
define('SENDMAIL_EMPTY_DATA', 1);
define('SENDMAIL_NO_SENDTO', 2);
define('SENDMAIL_NO_SENDFROM', 4);
define('SENDMAIL_ERROR', 16);

class ModuleFormBooking extends Module {
	/**
	* Template
	*
	* @var string
	*/
  protected $strTemplate = 'mod_seminarbooking';
  //
  protected $arrFields = array();
  //
  protected $bookingStatus = true;
  protected $reservationStatus = false;
  //
  public function generate () {
	if (TL_MODE == 'BE') {
		$objTemplate = new BackendTemplate('be_wildcard');

		$objTemplate->wildcard = '### SEMINARBUCHUNG ###';
		$objTemplate->title = $this->headline;
		$objTemplate->id = $this->id;
		$objTemplate->link = $this->name;
		$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

		return $objTemplate->parse();
	}
	//Load onChangeEvent()
	$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/seminarverwaltung/assets/seminarverwaltung.js';

	return parent::generate();  
  }

  public function compile () {
    $this->loadLanguageFile('tl_seminar_booking');
    $time = time();
    if (!empty($this->sv_seminar)) {
	    $seminaridx = $this->sv_seminar;
    } else {
    	$seminaridx = $this->Input->get('seminaridx');
	    if (empty($seminaridx)) {
	    	$seminaridx = $this->Input->get('seminar');
		}
    }
    $eventidx = $this->Input->get('eventidx');
    if (empty($eventidx) && $eventidx == '') {
	    $eventidx = $this->Input->get('event');
    }
    if (empty($eventidx)) {
	    $eventidx = 0;
    }
    $intStart = $this->Input->get('intStart');
    if (empty($intStart)) {
	    $intStart = $this->Session->get('intStart');
    }
    if (empty($intStart)) {
	    $intStart = 0;
    }
    $intEnd = $this->Input->get('intEnd');
    if (empty($intEnd)) {
	    $intEnd = $this->Session->get('intEnd');
    }
    if (empty($intEnd)) {
	    $intEnd = 0;
    }
   	$doSeminarShow = false;
   	$doSeminarSelect = false;
   	$doSelect = false;

    if ($seminaridx > 0) {  
    	$doSeminarShow = true;
    }
    if ($eventidx == 0) {  
    	$doSeminarSelect = true;
        if ($seminaridx == 0) {
	    	$vsemkat    =  $this->Input->post('seminar_kategorie');
	    	$doSeminarSelect = false;
	    } 
	    $vseminar   =  $this->Input->post('seminar');
	    if (empty($vseminar)) {
	    	$vseminar = $this->Session->get('seminarid');
	    }
	    if ($vseminar) {
		    $arrSeminarData = explode('_',$vseminar);
		    $seminardate = $arrSeminarData[0];
		    $seminaridx  = $arrSeminarData[1];
		    $eventidx    = $arrSeminarData[2];
	    }
	    // Seminar email info
	    $mail = '';
	    $bookingStatus = false;
	    $reservationStatus = false;
	    $places = 0;
	    if (!empty($vseminar)) {
		   $sql = "SELECT * FROM tl_seminar WHERE id=?";
	   		$objSeminar = $this->Database->prepare($sql)->limit(1)->execute($vseminar);
	    	if ($objSeminar->numRows) {
		   	 $mail = $objSeminar->email;
		   	 $places = $objSeminar->places;
	    	}
		}
		if ($this->sv_booking) {
		   	 $bookingStatus = true;
		   	 $reservationStatus = false;
		 } else {
		   	 $bookingStatus = false;
		   	 $reservationStatus = true;
		 }
	    $time = time();
		$showDate = $time;
		$arrDebug = array();
		
		// Event Infos
		$places_booked = 0;
		$doSelect = true;
    } else {
		$doSelect = false;
		
		$sql = "SELECT * FROM tl_seminar WHERE id=?";
		$objSeminar = $this->Database->prepare($sql)->limit(1)->execute($seminaridx);
		if ($objSeminar->numRows) {
		    $mail = $objSeminar->email;
		    $places = $objSeminar->places;
		    $showalways =$objSeminar->showalways;
		}
		if ($this->sv_booking) {
		   	 $bookingStatus = true;
		   	 $reservationStatus = false;
		 } else {
		   	 $bookingStatus = false;
		   	 $reservationStatus = true;
		}
		$time = time();
		$showDate = $time;
		$arrDebug = array();
		// Event Infos
		$places_booked = 0;
		$hasEvt = false;
		$show = false;
		$sql = "SELECT * FROM tl_seminar_events WHERE id=?";
		$objEvent = $this->Database->prepare($sql)->limit(1)->execute($eventidx);
		if ($objEvent->numRows) {
		    $intStart = ($intStart ? $intStart : $objEvent->date);
		    $intEnd = ($intEnd ? $intEnd : $objEvent->endDate);
		    $startDate = date($GLOBALS['TL_CONFIG']['dateFormat'],$intStart);
		    $details = $this->restoreBasicEntities($objEvent->details);
		    if ($intEnd <= $intStart) {
			    $endDate = null;
		    } else {
			    $endDate = date($GLOBALS['TL_CONFIG']['dateFormat'],$intEnd);
			}
		    if ($objSeminar->last_request_days) {
		        if ($objSeminar->last_request_direction == 'after') {
			    	$showDate = $intStart + $objSeminar->last_request_days*24*60*60;
			    } else {
			       	$showDate = $intStart - $objSeminar->last_request_days*24*60*60;
			    }
		    } else {
			    $showDate = $intStart;
			}
			$places_booked = $objEvent->places_booked;
		    $hasEvt = true;
		} 
		// Wenn Event vorhanden, dann prüfen, ob Plätze schon belegt oder generell =0 ==>Buchungen immer zulassen 
		if ($hasEvt) {
		    if (($places == 0) || (($places > 0) && ($places_booked < $places))) {
			    $show = true;
		    } else {
			    $show = false;
		    }
		}
		//
		// Seminarinfo aufbereiten
		$strBuf = $this->getSeminardaten($seminaridx,$eventidx,$intStart,$intEnd);
		// Wenn Termin in der Vergangenheit liegt bzw. nicht im Rahmen des Zeitversatzes, dann Buchung ausschalten
		if (($time > $showDate) && $show && !showalways) {
		    $strBuf .= $GLOBALS['TL_LANG']['MSC']['noBooking'];
		    $show = false;
		}
	}
	// GR 08-02-2014  
	if ($showalways) {
	// prüfen, ob immer anzeigen zugelassen, dann auch Buchen zulassen
	    $show = true;
	}

   	// Das Formular
	
	$vvorname  =	$this->Input->post('vorname');
	$vnachname =	$this->Input->post('nachname');
    $vgeschlecht =	$this->Input->post('geschlecht');
	$vstrasse  =	$this->Input->post('strasse');
	$vplz      =	$this->Input->post('plz');
	$vort      =	$this->Input->post('ort');
	$vtelefon  =	$this->Input->post('telefon');
    $vmobile   =	$this->Input->post('mobile');
    $vfax      =	$this->Input->post('fax');
	$vemail    =	$this->Input->post('email');
	$vagb      =	$this->Input->post('agb');
	$vmsg      =	$this->Input->post('msg');
	$vwiderruf =	$this->Input->post('widerruf');
	
	// Seminarkategorie
	$semkat = new FormSeminarcategorySelect();
  	$semkat->id    = 'seminar_kategorie';
  	$semkat->name  = 'seminar_kategorie';
  	$semkat->label = $GLOBALS['TL_LANG']['MSC']['seminar_category']; //'Seminarkategorie';
	// Seminarselect
	$seminar = new FormSeminarSelect($seminaridx);
  	$seminar->id    = 'seminar';
  	$seminar->name  = 'seminar';
  	$seminar->label = $GLOBALS['TL_LANG']['MSC']['seminar']; //'Seminar';
    // Seminardaten lesen
	// AGB akezptiert
  	$agb = new FormCheckBox();
  	$agb->id = 'agb';
  	$agb->name = 'agb';
  	$agb->label = $GLOBALS['TL_LANG']['MSC']['acceptAGB']; //'Ich habe die AGB gelesen und akzeptiert';
  	$agb->mandatory = true;
	// Widerruf akezptiert
  	$widerruf = new FormCheckBox();
  	$widerruf->id = 'widerruf';
  	$widerruf->name = 'widerruf';
  	$widerruf->label = $GLOBALS['TL_LANG']['MSC']['acceptWiderruf']; //'Ich habe die Widerrufklauseln gelesen und akzeptiert';
  	$widerruf->mandatory = true;
  	//$agb->mandatory = true;
  	// Benutzerdaten
  	$vorname = new FormTextField();
  	$vorname->name = 'vorname';
  	$vorname->label = $GLOBALS['TL_LANG']['MSC']['seminar_firstname']; //'Vorname';
  	$vorname->mandatory = true;
		//
  	$nachname = new FormTextField();
  	$nachname->name = 'nachname';
  	$nachname->label = $GLOBALS['TL_LANG']['MSC']['seminar_lastname']; //'Nachname';
  	$nachname->mandatory = true;
	//
	$geschlecht = new FormRadioButton(); //FormSelectMenu();
	$geschlecht->id   = 'geschlecht';
  	$geschlecht->name = 'geschlecht';
	//$geschlecht->label = $GLOBALS['TL_LANG']['MSC']['seminar_gender']; //'Anrede';
	$geschlecht->options = array(array('value'=>'m','label'=>$GLOBALS['TL_LANG']['MSC']['anredeMann']),array('value'=>'w','label'=>$GLOBALS['TL_LANG']['MSC']['anredeFrau']));
	$geschlecht->mandatory = true;
	//
  	$strasse = new FormTextField();
  	$strasse->name = 'strasse';
  	$strasse->label = $GLOBALS['TL_LANG']['MSC']['seminar_street']; //'Strasse';
  	$strasse->mandatory = true;
	//
  	$plz = new FormTextField();
  	$plz->name = 'plz';
  	$plz->label = $GLOBALS['TL_LANG']['MSC']['seminar_postal']; //'Plz';
  	$plz->mandatory = true;
  	$plz->rgxp = 'digit';
	//
  	$ort = new FormTextField();
  	$ort->name = 'ort';
  	$ort->label = $GLOBALS['TL_LANG']['MSC']['seminar_city']; //'Ort';
  	$ort->mandatory = true;
	//
  	$telefon = new FormTextField();
  	$telefon->name = 'telefon';
  	$telefon->label = $GLOBALS['TL_LANG']['MSC']['seminar_phone']; //'Telefon';
  	$telefon->mandatory = false;
	//
	$mobile = new FormTextField();
	$mobile->name = 'mobile';
	$mobile->label = $GLOBALS['TL_LANG']['MSC']['seminar_mobile']; //'Mobilnummer';
	$mobile->mandatory = false;
	//
	$fax = new FormTextField();
	$fax->name = 'fax';
	$fax->label = $GLOBALS['TL_LANG']['MSC']['seminar_fax']; //'Fax';
	$fax->mandatory = false;
	//
  	$email = new FormTextField();
  	$email->name = 'email';
  	$email->label = $GLOBALS['TL_LANG']['MSC']['seminar_email']; //'E-Mail';
  	$email->rgxp = 'email';
  	$email->mandatory = true;
	//
	$msg = new FormTextArea();
	$msg->name = 'msg';
	$msg->label = $GLOBALS['TL_LANG']['MSC']['seminar_remark']; //'Mitteilung ';
	$msg->mandatory = false;
	//
    $objTemplate = new FrontendTemplate($this->sv_seminarbooking_template);
    $objTemplate->bookingStatus = $bookingStatus; // GR 12.03.2014
    $objTemplate->doSelect = $doSelect;
    $objTemplate->doSeminarSelect = $doSeminarSelect;	
    // Formular submitted
	if ($this->Input->post('FORM_SUBMIT') == 'seminarbuchung') {
		// validate
		$blnValid = true;
	    if ($_POST && !$vagb) {
			$agb->addError($GLOBALS['TL_LANG']['MSC']['notAcceptedAGB']); //'Sie haben die AGB noch nicht akzeptiert.'
			$blnValid = false;
		} 
		if (empty($seminaridx) || ($seminaridx == 0)) { // || empty($eventidx) || ($eventidx == 0)) { // GR 12.03.2014
			$seminar->addError($GLOBALS['TL_LANG']['MSC']['noSeminarSelected']);
			$blnValid = false;
		}
		//if ($_POST && !$widerruf->validate()) {
		//	$widerruf->addError($GLOBALS['TL_LANG']['MSC']['notAcceptedWiderruf']); //'Sie haben die Widerrufklauseln noch nicht akzeptiert.'
		//	$blnValid = false;
		//} 
  		$this->Template->submitted = false;
		// Weiterverarbeiten, wenn fehlerfrei
		//if ($vagb && $vwiderruf) { 
		if ($blnValid) { 
			 //
			 // save Data to tl_seminar_booking table
			 //
			 $arrData = array();
			 $arrData = $this->getSeminarArray($seminaridx,$eventidx);
			 $arrData['lastname'] = $vnachname;
			 $arrData['firstname'] = $vvorname;
			 $arrData['gender'] = $vgeschlecht;
			 $arrData['street'] = $vstrasse;
			 $arrData['postal'] = $vplz;
			 $arrData['city'] = $vort;
			 $arrData['phone'] = $vtelefon ? $vtelefon : '';
			 $arrData['mobile'] = $vmobile ? $vmobile : '';
			 $arrData['fax'] = $vfax ? $vfax : '';
			 $arrData['email'] = $vemail;
			 // set reservation state
			 if ($reservationStatus || (($eventidx == 0) && ($seminaridx > 0))) { // GR 12.03.2014
			 	$arrData['reservation'] = true;
			 	$arrData['reservation_date'] = time();
			 } else if ($bookingStatus) {
			 // set booking state
			 	$arrData['booking'] = true;
			 	$arrData['booking_date'] = time();
			 }
			 $arrData['remark'] = $vmsg;
			 $arrData['tstamp'] = time();
			 //$arrData['tstamp'] = time();
			 // set message Text for mail and frontend
			 $strBuffer = $this->sv_messageText;
	         $strData .= $this->getSeminardaten($seminaridx,$eventidx).'</p><br>';
	         $strData .= '<p><strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_firstname'].'</strong> '.$vvorname.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_lastname'].'</strong> '.$vnachname.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_street'].'</strong> '.$vstrasse.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_postal'].'</strong> '.$vplz.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_city'].'</strong> '.$vort.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_phone'].'</strong> '.$vtelefon.'<br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_email'].'</strong> '.$vemail.'</p><br>';
	         $strData .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_remark'].'</strong> '.$vmsg.'</p><br>';
	         // replace insert tags
	         $anrede = $arrData['gender'] == 'm' ? $GLOBALS['TL_LANG']['tl_seminar_booking']['salutationMan'] : $GLOBALS['TL_LANG']['tl_seminar_booking']['salutationWoman'];
	         $strBuffer = str_replace('{{sv::salutation}}',$anrede,$strBuffer);
	         $strBuffer = str_replace('{{sv::firstname}}',$arrData['firstname'],$strBuffer);
	         $strBuffer = str_replace('{{sv::lastname}}',$arrData['lastname'],$strBuffer);
	         $strBuffer = str_replace('{{sv::title}}',$arrData['title'], $strBuffer);
	         $strBuffer = str_replace('{{sv::data}}',$strData, $strBuffer);
	         $strBuffer = str_replace('{{sv::intern}}',$arrData['intern'], $strBuffer);
	         $strBuffer = str_replace('{{sv::startDate}}',date($GLOBALS['TL_CONFIG']['dateFormat'],$arrData['startDate']), $strBuffer);
	         $strBuffer = str_replace('{{sv::cost}}',$arrData['cost'], $strBuffer);
	         $strBuffer = str_replace('{{sv::currency}}',$arrData['currency'], $strBuffer);
			 // text for frontend
	         $strBuffer = $this->replaceInsertTags($strBuffer);
	         $strBufferShow = $strBuffer;
             // and next part for mail message
	         $strDetails .= $details ? $details : '';
	         // replace insert tags
	         $strBuffer = str_replace('{{sv::description}}',$strDetails,$strBuffer);
			 //
			 // Prüfung der E-Mailadressen
			 //
			 $errmail = false;
			 $sender = trim($vemail);
			 $bccmail  = trim($this->sv_bccMail);
			 if (empty($sender)) {
				 $strBuf .= '<div class="error">'.$GLOBALS['TL_LANG']['MSC']['emailWrong'].'</div>';
				 $errmail = true;
			 } else {
	 	    	if (!strpos($bccmail,'@') || (empty($bccmail))) {
		 			$bccmail = $GLOBALS['TL_CONFIG']['adminEmail'];
		 			if (empty($bccmail)) {	
			 			$strBuf .= '<div class="error">'.$GLOBALS['TL_LANG']['MSC']['bccWrong'].'</div>';
			 			$errmail = true;
				 	}
				 }
			 }
			 // 
			 // Mail versenden, Buchung in die Datenbank eintragen und Log schreiben
			 //
			 if (!$errmail) {

		         $subject = $this->sv_shortText;
		         $subject = str_replace('{{sv::salutation}}',$anrede,$subject);
		         $subject = str_replace('{{sv::firstname}}',$arrData['firstname'],$subject);
		         $subject = str_replace('{{sv::lastname}}',$arrData['lastname'],$subject);
		         $subject = str_replace('{{sv::title}}',$arrData['title'], $subject);
		         $subject = str_replace('{{sv::data}}',$strData, $subject);
		         $subject = str_replace('{{sv::intern}}',$arrData['intern'], $subject);
		         $subject = str_replace('{{sv::startDate}}',date($GLOBALS['TL_CONFIG']['dateFormat'],$arrData['startDate']), $subject);
		         $subject = str_replace('{{sv::cost}}',$arrData['cost'], $subject);
		         $subject = str_replace('{{sv::currency}}',$arrData['currency'], $subject);
		         $subject = str_replace('{{sv::description}}',$strDetails,$subject);
		         $subject = $this->replaceInsertTags($subject);
				 $subject = html_entity_decode($subject);

		         $sendmail = new Email();
		         $sendmail->fromName = $this->sv_fromText;
		         $sendmail->from = $bccmail;
		         $sendmail->subject = $subject;
				 $sendmail->charset = 'UTF-8';
				 $sendmail->priority = 'normal';
		         $sendmail->html = $strBuffer;
		         $sendmail->text = strip_tags(str_replace('<br>',"\n",$strBuffer));
				 //$sendmail->attachFile('tl_files/tanzimpulse/downloads/Tanzimpulse AGB.pdf');
				 //$sendmail->attachFile('tl_files/tanzimpulse/downloads/Tanzimpulse Widerruf.pdf');
		         if ($mail) {
		         	$sendmail->sendCc($mail);    // email address form seminar settings
		         }
		         $sendmail->sendBcc($bccmail);    // email address form seminar settings
		         $sendmail->replyTo($bccmail);    // answer email address from module settings
		         $sendmail->sendTo($sender);
				 // jump to Ready Page - this time the same Page 
		         $strBufferShow .= '<p><strong>'.$GLOBALS['TL_LANG']['MSC']['emailMessage'].'</strong></p>';
				 $objTemplate->ready = $strBufferShow;
				 $objTemplate->submitted = true;
				 //
				 // save data to tl_seminar_booking table
				 //
				 // set data
				 $sql = "INSERT INTO tl_seminar_booking %s";
				 $this->Database->prepare($sql)->set($arrData)->execute();
				 // Log schreiben
				 $this->writeLog($arrData);
				 //
				 // update booking/reservation data to tl_seminar_events table
				 //
				 $arrStatus = array();
				 $places_requested = 0;
				 $places_booked = 0;
				 $sqlevtdata = "SELECT * FROM tl_seminar_events WHERE id=?";
				 $objEvt = $this->Database->prepare($sqlevtdata)->execute($eventidx);
				 if ($objEvt->numRows) {
					 $places_requested = $objEvt->places_requested;
					 $places_booked    = $objEvt->places_booked;
				 }
				 
				 if ($this->bookingStatus) {
					 $places_booked++;
					 $arrStatus['places_booked'] = $places_booked;
				 }
				 if ($this->reservationStatus) {
					 $places_requested++;
					 $arrStatus['places_requested'] = $places_requested;
				 }
				 if ($eventidx > 0) { // GR 12.03.2014
				 if (!empty($arrStatus)) {
				 	$sqlevtupd = "UPDATE tl_seminar_events %s WHERE id=?";
				 	$this->Database->prepare($sqlevtupd)->set($arrStatus)->execute($eventidx);
				 }
				 } // GR 12.03.2014
			 }
		}
	}	
	// HIDDEN Parameter setzen
	$this->Input->setPost('show_recurring',($this->sv_show_recurring?'1':'0'));
    // Werte/Defaultwerte belegen
  	$semkat->value = $vsemkat;
  	$seminar->value = $vseminar;
  	$vorname->value = empty($vvorname)? '': $vvorname;
 	$nachname->value = empty($vnachname)? '': $vnachname;
	$geschlecht->value = empty($vgeschlecht)? '': $vgeschlecht;
	$strasse->value = empty($vstrasse)? '': $vstrasse;
	$plz->value = empty($vplz)? '': $vplz;
	$ort->value = empty($vort)? '': $vort;
	$telefon->value = empty($vtelefon)? '': $vtelefon;
	$mobile->value = empty($vmobile)? '': $vmobile;
	$fax->value = empty($vfax)? '': $vfax;
	$email->value = empty($vemail)? '': $vemail;
	$agb->value = $vagb? true : false;
	$msg->value = empty($vmsg)? '' : $vmsg;
    // ﾜbergabe ans Formular
    $objTemplate->semkat = $semkat;
    $objTemplate->seminar = $seminar;
	$objTemplate->agb = $agb;
	$objTemplate->widerruf = $widerruf;
	$objTemplate->vorname = $vorname;
	$objTemplate->nachname = $nachname;
	$objTemplate->geschlecht = $geschlecht;
	$objTemplate->strasse = $strasse;
	$objTemplate->plz = $plz;
	$objTemplate->ort = $ort;
	$objTemplate->telefon = $telefon;
	$objTemplate->mobile = $mobile;
	$objTemplate->fax = $fax;
	$objTemplate->email = $email;
	$objTemplate->msg = $msg;
	// Template-Variablen
    $objTemplate->arrSeminar = $this->getSeminarArrayComplete($seminaridx,$eventidx);
	$objTemplate->action = $this->Environment->request; 
	$this->Template->formular = $objTemplate->parse();
  }

  private function insertTableData() {
	  
  }
  private function updateTableData() {
	  
  }
  private function writeLog($arrData,$strFile=NULL,$blnLog=false) {
	  // Globale Variable aus Konfiguration prüfen
	  //..
	  if (($GLOBALS['TL_CONFIG']['sv_booking_log'] === true)||($blnLog === true))  {
		  // Basis-Verzeichnisse prüfen Contao 2.1x "tl_file", Contao 3.x "file"
		  if (is_dir("files")) {
		     if ($strFile === NULL) {
			     $strFile = 'files/log_booking.csv';
		     }
			 $this->writeCSV($strFile,$arrData,'create');
		  } else if (is_dir("tl_files")) {
		     if ($strFile === NULL) {
			     $strFile = 'tl_files/log_booking.csv';
		     }
			 $this->writeCSV($strFile,$arrData,'create');
		  } else {
			 $this->log("Logfile for booking not writable",__METHOD__,"WARN");
		  }
	  }
  }
  private function sendMail($arrMailData,$strFrom,$strFromName,$strTo,$strCC,$strBCC,$strReplyTo) {
	$retcode = SENDMAIL_ERROR;
  	$subject   = $arrData['subject'];
  	$text      = $arrData['text'];
  	$html      = $arrData['html'];
  	$priority  = $arrData['priority'];
  	$charset   = $arrData['charset'];
  	$arrAttach = $arrData['arrAttach'];
	// ...
	//
	// ...	  
	$retcode = SENDMAIL_OK;
	return $retcode;
  }
  private function getSeminarCategoryDataArray() {
	  
  }
  private function getSeminarDataArray() {
	  
  }
  private function getSeminarEventDataArray() {
	  
  }
  private function getFormDataArray() {
	  
  }
  private function setFormDataArray() {
	  
  }
  private function getJumpToUrl() {
	  
  }
  private function getInsertTags() {
	  
  }

  public function writeCSV($fname,$arrData,$flag) {
	  $output = '';
	  if (!file_exists($fname)) {
	    $output .= '"action"';
	    foreach($arrData as $k=>$v) {
	       $output .= ';"'.$k.'"';
	    }
	  	$output .= "\n"; 
	  }
	  $output .= '"'.$flag.'"';
	  foreach($arrData as $k=>$v) {
	    $output .= ';"'.str_replace("\n","<br>",$v).'"';
	  }
	  $output .= "\n"; 
	  //$output .= implode(";",$arrData)."\n";
	  $datei = fopen($fname,"a");
	  fwrite($datei,$output);
	  fclose($datei);
  } 

  public function getSeminardaten($idx,$evtIdx) {
	  $pos = strpos($idx,'_')+1;
	  $id = substr($idx,$pos);
	  $sql = "SELECT cost,reducedcost,currency,specials,title,intern,location FROM tl_seminar WHERE id=?";
	  $result = $this->Database->prepare($sql)->execute($idx);
	  if ($result->numRows) {
	      $specials = str_replace(array('[&]', '[&amp;]', '[lt]', '[gt]', '[nbsp]', '[-]'), array('&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&shy;'), $result->specials); 
	      $data  = '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_title'].'</strong> '.$result->title.'<br>';
	      if (!empty($evtIdx)) {
			  $sqlevt = "SELECT intern,details,date,endDate FROM tl_seminar_events WHERE id=?";
	   		  $resevt = $this->Database->prepare($sqlevt)->execute($evtIdx);
		      if ($resevt->numRows) {
				$data .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_intern'].'</strong> '.$resevt->intern.'<br>';
				if (!empty($resevt->date)) {
	  		      	$data .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_timerange'].'</strong> '.$this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$resevt->date);
					if (!empty($resevt->endDate)) {		
	  			  		if ($resevt->endDate > $resevt->date) {
				      		$data .= ' - '.date($GLOBALS['TL_CONFIG']['dateFormat'],$resevt->endDate).'<br>';
					  	}
					}
				}
		      }
	      }
	      $data .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_cost'].'</strong> '.$result->currency.' '.$result->cost.'<br>';
	      $data .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_location'].'</strong> '.$result->location.'<br>';
	      $data .= '<strong>'.$GLOBALS['TL_LANG']['MSC']['seminar_specials'].'</strong> '.$specials.'<br>';
	  } else {
	      $data = $GLOBALS['TL_LANG']['MSC']['noSeminarData'].'<br>';
	  }
	  return $data;
  }

  public function getSeminarArray($idx,$evtIdx) {
	  $pos = strpos($idx,'_')+1;
	  $id = substr($idx,$pos);
	  $arrData = array();
	  $sql = "SELECT cost,reducedcost,currency,details,specials,title,location,intern FROM tl_seminar WHERE id=?";
	  $result = $this->Database->prepare($sql)->execute($idx);
	  if ($result->numRows) {
          $arrData['seminarid'] = $idx;
		  $arrData['intern'] = $result->intern;
		  //$arrData['startDate'] = NULL;      // GR 12.03.2014
	      $arrData['cost'] = $result->cost;
	      //$arrData['reducedcost'] = $result->reducedcost;
	      $arrData['currency'] = $result->currency;
	      //$arrData['details'] = $result->details;
	      //$arrData['specials'] = $result->specials;
	      $arrData['title'] = $result->title;
	      //$arrData['location'] = $result->location;
	      if (!empty($evtIdx)) {
	          $arrEvt = array();
	          $arrData['pid'] = $evtIdx;
			  $sqlevt = "SELECT intern,details,date,endDate FROM tl_seminar_events WHERE id=?";
	   		  $resevt = $this->Database->prepare($sqlevt)->execute($evtIdx);
		      if ($resevt->numRows) {
				  $arrData['intern'] = $resevt->intern;
				  $arrData['startDate'] = $resevt->date;
				  //$arrData['endDate'] = $resevt->endDate;
				  //$arrData['eventDetails'] = $resevt->details;
		      }
	      }
	  }
	  return $arrData;
  }
  public function getSeminarArrayComplete($idx,$evtIdx) {
	  $pos = strpos($idx,'_')+1;
	  $id = substr($idx,$pos);
	  $arrData = array();
	  $sql = "SELECT * FROM tl_seminar WHERE id=?";
	  $result = $this->Database->prepare($sql)->execute($idx);
	  if ($result->numRows) {
	      $arrData['cost'] = $result->cost;
	      $arrData['reducedcost'] = $result->reducedcost;
	      $arrData['currency'] = $result->currency;
	      $arrData['details'] = $result->details;
	      $arrData['specials'] = $result->specials;
	      $arrData['title'] = $result->title;
	      $arrData['location'] = $result->location;
	      $arrData['showalways'] = $result->showalways;
	      $arrData['duration'] = $result->duration;
	      $arrData['places'] = $result->places;
	      $arrData['places_min'] = $result->places_min;
	      $arrData['showalways'] = $result->showalways;
	      if (!empty($evtIdx)) {
	          $arrEvt = array();
	          $arrData['pid'] = $evtIdx;
			  $sqlevt = "SELECT * FROM tl_seminar_events WHERE id=?";
	   		  $resevt = $this->Database->prepare($sqlevt)->execute($evtIdx);
		      if ($resevt->numRows) {
				  $arrData['arrEvent'] = $resevt->row();	
				  $arrData['intern'] = $resevt->intern;
				  $arrData['startDate'] = $resevt->date;
				  $arrData['endDate'] = $resevt->endDate;
				  $arrData['startTime'] = $resevt->startTime;
				  $arrData['endTime'] = $resevt->endTime;
				  $arrData['eventDetails'] = $resevt->details;
				  $arrData['eventSpecials'] = $resevt->specials;
				  $arrData['reservations'] = $resevt->places_requested;
				  $arrData['booked'] = $resevt->places_booked;
		      }
	      }
	  }
	  return $arrData;
  }
  public function getSeminarEventlist($seminarId,$eventId=0) {
		$time = time();
		if (isset($seminarId)) {
			$sql = "SELECT title,teaser,details,specials,location,duration,cost,currency,reducedcost FROM tl_seminar WHERE id=?".(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			
			$arrSem = array();
			$arrEvt = array();
			$result = $this->Database->prepare($sql)->execute($seminarId);
			if ($result->numRows) {
				$arrSem['title'] = $result->title;
				$arrSem['teaser'] = $this->replacePlaceholders($result->teaser);
				$arrSem['details'] = $this->replacePlaceholders($result->details);
				$arrSem['specials'] = $this->replacePlaceholders($result->specials);
				$arrSem['location'] = $result->location;
				$arrSem['duration'] = $result->duration;
				$arrSem['cost'] = $result->cost;
				$arrSem['currency'] = $result->currency;
				$arrSem['reducedcost'] = $result->reducedcost;
				if (isset($eventId)) {
					$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((date>=$time) OR (date<=$time AND endDate>=$time) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time) AND date<=$time))" . 
								(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . 
								" ORDER BY date";
					//$sqlEvt = "SELECT intern,details,date,endDate,startTime,endTime,addTime FROM tl_seminar_events WHERE id=?";
					$objEvt = $this->Database->prepare($sqlEvt)->limit(1)->execute($eventId);
					if ($objEvt->numRows) {
						$arrEvt['intern'] = $objEvt->intern;
						$arrEvt['evtDetails'] = $this->replacePlaceholders($objEvt->details);
						$arrEvt['startDate'] = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->date);
						if ($objEvt->multipleDate) {
							$arrEvt['endDate'] = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->endDate);
						} else {
							$arrEvt['endDate'] = NULL;
						}
						if ($objEvent->addTime) {
							$arrEvt['startTime'] = $objEvt->startTime;
							$arrEvt['endTime'] = $objEvt->endTime;
						} else {
							$arrEvt['startTime'] = "";
							$arrEvt['endTime'] = "";
						}
					}
				}
				$jsondata = array_merge($arrSem,$arrEvt);
			} else {
				$noData = $GLOBALS['TL_LANG']['MSC']['seminar_noData'];
				$jsondata = array($noData,"","","","","","","","","","","","","","");
			}
			echo json_encode($jsondata);
		}
  }
  private function replacePlaceholders($str) {
		$data = str_replace(array('[&]', '[&amp;]', '[lt]', '[gt]', '[nbsp]', '[-]'), array('&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&shy;'), $str); 
		return $data;
  }

}

?>