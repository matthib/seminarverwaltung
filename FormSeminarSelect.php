<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

class FormSeminarSelect extends FormSelectMenu {
	
	protected $arrData;
	protected $seminarid;
	
	public function __construct($seminarid=0,$blnRecurring=0) {
		parent::__construct();
		if ($seminarid) {
			$this->seminarid = $seminarid;
		}
		$time = time();
		$intStart = $time;
		$intEnd = strtotime("+3 Month",$intStart);
		$this->arrData = array();
		$this->Import('Database');
		$sqlCat = "SELECT * FROM tl_seminar_category ORDER BY title ASC";
		$objCat = $this->Database->prepare($sqlCat)->execute();
		$arrSem = array();
	    while ($objCat->next()) {
	    	$catid    = $objCat->id;
	    	$cattitle = $objCat->title;
	    	//$sqlSem = "SELECT * FROM tl_seminar WHERE pid=?";
	    	$sqlSem = "SELECT * FROM tl_seminar WHERE pid=?".(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objSem = $this->Database->prepare($sqlSem)->execute($catid);
			$i = 0;
			$arrSem['0_0_0'] = $GLOBALS['TL_LANG']['MSC']['seminar_choose'];
	    	while ($objSem->next()) {
	    		$semid = $objSem->id;
	    		$semtitle    = $objSem->title;
	    		$seminternal = $objSem->intern;
				$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((date>=$time) OR (date<=$time AND endDate>=$time) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time) AND date<=$time))" . 
							(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . 
							" ORDER BY date";
	    		//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? ORDER BY date ASC";
	    		$objEvt = $this->Database->prepare($sqlEvt)->execute($semid);
	    		while ($objEvt->next()) { 
	    			$evtdate = $objEvt->date;
	    			$keydate = date("Ymd",$evtdate);
	    			$evtid = $objEvt->id;
					$startDt = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$evtdate);
	    			$idx = $keydate . '_' . $semid . '_' . $evtid;
					$arrSem[$idx] = $startDt . ' / '.$semtitle.' ('.$objEvt->intern.')';
					if ($blnRecurring) {
		    			if ($objEvt->recurring) {
		    				$count = 0;
							$arrRepeat = deserialize($objEvt->repeatEach);
							if (!empty($arrRepeat)) {
								$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];
								if ($arrRepeat['value'] < 1) {
									continue;
								}
								while ($objEvt->endTime < $intEnd) {
									if ($objEvt->recurrences > 0 && $count++ >= $objEvt->recurrences) {
										break;
									}
									$objEvt->startTime = strtotime($strtotime, $objEvt->startTime);
									$objEvt->endTime = strtotime($strtotime, $objEvt->endTime);
									// Skip events outside the scope
									if ($objEvt->endTime < $intStart || $objEvt->startTime > $intEnd)	{
										continue;
									}
					    			$keydate = date("Ymd",$objEvt->startTime);
									$startDt = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->startTime);
					    			$idx = $keydate . '_' . $semid . '_' . $evtid;
									$arrSem[$idx] = $startDt . ' / '.$semtitle.' ('.$objEvt->intern.')';
				    			}
			    			}
			    		}
					}
	    		}
	    	}
		}
		// sortieren der Seminare nach Datum
		if (!empty($arrSem)) {
			ksort($arrSem);
		}
		$this->arrData = array_merge($this->arrData,$arrSem); 

	}
	
	public function generate() {
		$strBuffer = parent::generate();
		if ($this->readonly || $this->disabled)
		   return $strBuffer;
		//Startwert aufarbeiten
		$startwert = $this->value;
		//
		$seminarid = 0;
		$strBuffer = '<select name="'.$this->name.'" onchange="onChangeEvent();">';
		foreach ($this->arrData as $key => $val) {
		    if ($this->seminarid) {
		        if (strpos($key,'_'.$this->seminarid.'_') || ($key === '0_0_0')) {
					if ($key == $startwert) {
						$strBuffer .= '<option selected="selected" value="'.$key.'">'.$val.'</option>';
					} else {
						$strBuffer .= '<option value="'.$key.'">'.$val.'</option>';
					}
				}
			} else {
				if ($key == $startwert) {
					$strBuffer .= '<option selected="selected" value="'.$key.'">'.$val.'</option>';
				} else {
					$strBuffer .= '<option value="'.$key.'">'.$val.'</option>';
				}
			}
		}
		$strBuffer .= '</select>';
		return $strBuffer;
	}
}
?>