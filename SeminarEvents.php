<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2012 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 */

/**
 * Class SeminarEvents
 *
 * @package    seminarverwaltung
 *
 * GR 17.12.2014 Key eindeutig mit EVENT ID versehen
 * GR 27.03.2015 Recurring Events Auswertung korrigiert, wenn endTime falsch gesetzt ist
 *
 */
abstract class SeminarEvents extends Module
{

	/**
	 * Current URL
	 * @var string
	 */
	protected $strUrl;

	/**
	 * Today 00:00:00
	 * @var string
	 */
	protected $intTodayBegin;

	/**
	 * Today 23:59:59
	 * @var string
	 */
	protected $intTodayEnd;

	/**
	 * Current events
	 * @var array
	 */
	protected $arrEvents = array();


	/**
	 * Sort out protected archives
	 * @param array
	 * @return array
	 */
	protected function sortOutProtected($arrCalendars) {
		if (BE_USER_LOGGED_IN || !is_array($arrCalendars) || empty($arrCalendars)) {
			return $arrCalendars;
		}

		$this->import('FrontendUser', 'User');
		$objCalendar = $this->Database->execute("SELECT id, protected, groups FROM tl_seminar_category WHERE id IN(" . implode(',', array_map('intval', $arrCalendars)) . ")");
		$arrCalendars = array();

		while ($objCalendar->next()) {
			if ($objCalendar->protected) {
				if (!FE_USER_LOGGED_IN)	{
					continue;
				}
				$groups = deserialize($objCalendar->groups);
				if (!is_array($groups) || empty($groups) || count(array_intersect($groups, $this->User->groups)) < 1) {
					continue;
				}
			}
			$arrCalendars[] = $objCalendar->id;
		}
		return $arrCalendars;
	}


	/**
	 * Get all events of a certain period
	 * @param array
	 * @param integer
	 * @param integer
	 * @return array
	 */
	protected function getAllEvents($arrCalendars, $intStart, $intEnd) {
		if (!is_array($arrCalendars)) {
			return array();
		}
		$this->import('String');
		$time = time();
		$this->arrEvents = array();

		foreach ($arrCalendars as $id) {
			$strUrl = $this->strUrl;
			$objCat = $this->Database->prepare("SELECT id,title FROM tl_seminar_category WHERE id=?")->limit(1)->execute($id);
			$catid = $id;
			if ($objCat->numRows) {
				$catid = $objCat->id;
				$catTitle = $objCat->title;
			}
			// Get current "jumpTo" page
			$objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=(SELECT sv_jumpTo FROM tl_seminar_category WHERE id=?)")
									  ->limit(1)
									  ->execute($id);
			if ($objPage->numRows) {
				$strUrl = $this->generateFrontendUrl($objPage->row(), ($GLOBALS['TL_CONFIG']['useAutoItem'] ?  '/%s' : '/events/%s'));
			}

			// Sprung zum Seminarreader ermitteln aus Modul
			$sqlModPg = "SELECT id, alias FROM tl_page WHERE id=?";
			$modJumpTo = $this->Database->prepare($sqlModPg)
	          	->execute($this->sv_jumpTo);
			if ($modJumpTo->numRows) {
				$tarPage = $this->generateFrontendUrl($modJumpTo->row());
			} else if ($this->sv_jumpTo) {
				$tarPage = $this->sv_jumpTo;
			} else {
				$tarPage = $strUrl;
			}
			$jt = $tarPage;

			$objSeminars = $this->Database->prepare("SELECT *, (SELECT title FROM tl_seminar_category WHERE id=?) AS calendar, (SELECT name FROM tl_user WHERE id=author) author FROM tl_seminar WHERE pid=? AND published=1")
										->execute($id, $id);
			if ($objSeminars->numRows < 1) {
				continue;
			}
			while ($objSeminars->next()) {
			    $pid = $objSeminars->id;
				
				// Get events of the current period
				$objEvents = $this->Database->prepare("SELECT * FROM tl_seminar_events WHERE pid=? AND ((startTime>=? AND startTime<=?) OR (endTime>=? AND endTime<=?) OR (startTime<=? AND endTime>=?) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=?) AND startTime<=?)) AND published=1" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . " ORDER BY startTime")
											->execute($pid, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd);
				if ($objEvents->numRows < 1) {
					continue;
				}

				while ($objEvents->next()) {
					$this->addEvent($objEvents, $objEvents->startTime, $objEvents->endTime, $jt, $intStart, $intEnd, $catid, $objSeminars->row(),$catTitle);

					//
					// Start -  GR 27.03.2015 Recurring Events Auswertung korrigiert, wenn endTime falsch gesetzt ist
					//
					// Recurring events
					if ($objEvents->recurring) {
						
						$arrRepeat = deserialize($objEvents->repeatEach);
	
						if (!empty($arrRepeat)) {
							if ($arrRepeat['value'] < 1)
							{
								continue;
							}
							// Korrektur, falls endTime falsch belegt ist.
							if (!$objEvents->multipleDates) {
								$endHour = date("H",$objEvents->endTime);
								$endMinute = date("i",$objEvents->endTime);
								$endDate = strToTime(date("Ymd",$objEvents->startTime)." 00:00:00");
								$objEvents->endTime = strtotime("+".$endHour." hours ".$endMinute." minutes",$endDate);
							}
							$count = 0;
							$intStartTime = $objEvents->startTime;
							$intEndTime = $objEvents->endTime;
							$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];
		
							while ($intStartTime <= $intEnd)
							{
								if ($objEvents->recurrences > 0 && $count++ >= $objEvents->recurrences)
								{
									break;
								}
								$intStartTime = strtotime($strtotime, $intStartTime);
								$intEndTime = strtotime($strtotime, $intEndTime);
		
								// Skip events outside the scope
								if ($intEndTime < $intStart || $intStartTime > $intEnd)
								{
									continue;
								}
		
								$this->addEvent($objEvents, $intStartTime, $intEndTime, $jt, $intStart, $intEnd, $catid, $objSeminars->row(),$catTitle);
							}
						}
					}
					//
					// Ende -  GR 27.03.2015 Recurring Events Auswertung korrigiert, wenn endTime falsch gesetzt ist
					//
				}
			}
		}

		// Sort the array
		foreach (array_keys($this->arrEvents) as $key) {
			ksort($this->arrEvents[$key]);
		}

		// HOOK: modify the result set
		if (isset($GLOBALS['TL_HOOKS']['getAllSVEvents']) && is_array($GLOBALS['TL_HOOKS']['getAllSVEvents'])) {
			foreach ($GLOBALS['TL_HOOKS']['getAllSVEvents'] as $callback) {
				$this->import($callback[0]);
				$this->arrEvents = $this->$callback[0]->$callback[1]($this->arrEvents, $arrCalendars, $intStart, $intEnd, $this);
			}
		}

		return $this->arrEvents;
	}

	/**
	 * Get all events of a certain period
	 * @param array
	 * @param integer
	 * @param integer
	 * @return array
	 */
	protected function getAllSeminarEvents($seminarId, $intStart, $intEnd) {
		$this->import('String');

		$time = time();
		$this->arrEvents = array();

		$strUrl = $this->strUrl;

		$pid = $seminarId;
		$objSeminars = $this->Database->prepare("SELECT * FROM tl_seminar WHERE id=?")->limit(1)->execute($pid);

		if ($objSeminars->numRows) {
		    $catid = $objSeminars->pid;	// Category Referenz
			$objCat = $this->Database->prepare("SELECT id,title FROM tl_seminar_category WHERE id=?")->limit(1)->execute($objSeminars->pid);
			if ($objCat->numRows) {
				$catid = $objCat->id;
				$catTitle = $objCat->title;
			}
			// Get current "jumpTo" page
			$objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=(SELECT sv_jumpTo FROM tl_seminar_category WHERE id=?)")
								  ->limit(1)
								  ->execute($id);
			if ($objPage->numRows) {
				$this->strUrl = $this->generateFrontendUrl($objPage->row(), ($GLOBALS['TL_CONFIG']['useAutoItem'] ?  '/%s' : '/event/%s'));          
			}
			// Sprung zum Seminarreader ermitteln aus Modul
			$sqlModPg = "SELECT id, alias FROM tl_page WHERE id=?";
			$modJumpTo = $this->Database->prepare($sqlModPg)
	          	->execute($this->sv_jumpTo);
			if ($modJumpTo->numRows) {
				$tarPage = $this->generateFrontendUrl($modJumpTo->row(),$strUrl);
			} else if ($this->sv_jumpTo) {
				$tarPage = $this->sv_jumpTo;
			} else {
				$tarPage = $strUrl;
			}
			$jt = $tarPage;

			// Get events of the current period
				$objEvents = $this->Database->prepare("SELECT * FROM tl_seminar_events WHERE pid=? AND ((startTime>=? AND startTime<=?) OR (endTime>=? AND endTime<=?) OR (startTime<=? AND endTime>=?) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=?) AND startTime<=?))" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . " ORDER BY startTime")
											->execute($pid, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd);

			while ($objEvents->next()) {
				$this->addEvent($objEvents, $objEvents->startTime, $objEvents->endTime, $jt, $intStart, $intEnd, $catid, $objSeminars->row(),$catTitle);

				// Recurring events
				if ($objEvents->recurring) {
					$count = 0;

					$arrRepeat = deserialize($objEvents->repeatEach);
					$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];

					if ($arrRepeat['value'] < 1) {
						continue;
					}

					while ($objEvents->endTime < $intEnd) {
						if ($objEvents->recurrences > 0 && $count++ >= $objEvents->recurrences) {
							break;
						}

						$objEvents->startTime = strtotime($strtotime, $objEvents->startTime);
						$objEvents->endTime = strtotime($strtotime, $objEvents->endTime);

						// Skip events outside the scope
						if ($objEvents->endTime < $intStart || $objEvents->startTime > $intEnd)	{
							continue;
						}
						$this->addEvent($objEvents, $objEvents->startTime, $objEvents->endTime, $jt, $intStart, $intEnd, $catid, $objSeminars->row(),$catTitle);
					}
				}
			}
		}

		// Sort the array
		foreach (array_keys($this->arrEvents) as $key) {
			ksort($this->arrEvents[$key]);
		}

		// HOOK: modify the result set
		if (isset($GLOBALS['TL_HOOKS']['getAllSeminarEvents']) && is_array($GLOBALS['TL_HOOKS']['getAllSeminarEvents'])) {
			foreach ($GLOBALS['TL_HOOKS']['getAllSeminarEvents'] as $callback) {
				$this->import($callback[0]);
				$this->arrEvents = $this->$callback[0]->$callback[1]($this->arrEvents, $arrCalendars, $intStart, $intEnd, $this);
			}
		}

		return $this->arrEvents;
	}

	/**
	 * Add an event to the array of active events
	 * @param Database_Result
	 * @param integer
	 * @param integer
	 * @param string
	 * @param integer
	 * @param integer
	 * @param integer
	 * @param array
	 * @param string
	 */
	protected function addEvent(Database_Result $objEvents, $intStart, $intEnd, $strUrl, $intBegin, $intLimit, $catid, $arrSeminar,$cattitle) {
		global $objPage;
		$intDate = $intStart;
		//
		// Korrektur EndeZeit + EndeDatum
		//
		$intDiff = $objEvents->endDate - $objEvents->date;
		$intEndX = $intEnd + $intDiff;
		$loc = $objEvents->location ? $objEvents->location : ($arrSeminar['location'] ? $arrSeminar['location'] : '' );
		$intKey = date('Ymd_Hi', $intStart).'_'.$loc;
		$span = Calendar::calculateSpan($intStart, $intEndX); //$intEnd);
		$strDate = $this->parseDate($objPage->dateFormat, $intStart);
		$strDay = $GLOBALS['TL_LANG']['DAYS'][date('w', $intStart)];
		$strMonth = $GLOBALS['TL_LANG']['MONTHS'][(date('n', $intStart)-1)];
//
// Start GR 08.02.2014
//
	      $intStart = ($intStart ? $intStart : $objEvents->startTime);
		  $intEnd = ($intEnd ? $intEnd : $objEvents->endTime);
	      $startTime = $objEvents->startTime; //$objEvents->startTime;
	      $endTime = $objEvents->endTime;
	      $intEnd = $endTime;
	      $intStart = $startTime;
	      $published = $objEvents->published;
	      $stop = $startDate + (24*60*60);

		  // Datum und Zeitformate ermitteln
		  $strDate = $this->parseDate($objPage->dateFormat,$startTime);

		  $daySec = 24*60*60; // Tag in Sekunden
		  $intStartDays = floor($startTime / $daySec); // Stardatum in Tagen
		  $intEndDays = floor($endTime / $daySec);     // Enddatum in Tagen
		  $diff = $intEndDays - $intStartDays;
		  if ($diff > 0) {
			$strDate = $this->parseDate($objPage->dateFormat, $startTime) . ' - ' . $this->parseDate($objPage->dateFormat, $endTime);
		  }

		  $strTime = '';

		  if ($objEvents->addTime) {
		    if (($endTime > $startTime) && ($diff > 0)) {
				$strDate = $this->parseDate($objPage->datimFormat, $startTime) . ' - ' . $this->parseDate($objPage->datimFormat, $endTime);
		    } else if ($startTime == $endTime) {
				$strTime = $this->parseDate($objPage->timeFormat, $startTime);
			} else {
				$strTime = $this->parseDate($objPage->timeFormat, $startTime) . ' - ' . $this->parseDate($objPage->timeFormat, $endTime);
			}
		  }
//
// Ende GR 08.02.2014
//
		// Store raw data
		$arrEvent = $objEvents->row();

		// Start GR 08.09.2014 
		if (empty($arrEvent['facilitator']) || $arrEvent['facilitator'] == 0) {
			$facilitator = $arrSeminar['facilitator'];
		} else {
			$facilitator = $arrEvent['facilitator'];
		}
		// Ende GR 08.09.2014 
		// Start GR 26.04.2014 
		$arrReferentData = $this->getReferentData($facilitator);
		$arrEvent['referent'] = $arrReferentData['referent'];
		$arrEvent['arrReferent'] = $arrReferentData['arrReferent'];
		// Ende GR 26.04.2014 

		// Zielseite vom ReaderModul aus ermitteln
		// $sql = "SELECT id,alias FROM tl_page WHERE id=(SELECT pid FROM tl_article WHERE id=(SELECT pid FROM `tl_content` WHERE module=?))";
		$sql0 = "SELECT pid FROM tl_content WHERE module=?";
		$objC = $this->Database->prepare($sql0)->execute($this->sv_cal_readerModule);
		if ($objC->numRows) {
			$cpid = $objC->pid;
			$sql1 = "SELECT pid FROM tl_article WHERE id=?";
			$objA = $this->Database->prepare($sql1)->execute($cpid);
			if ($objA->numRows) {
				$apid = $objA->pid;
				$sql = "SELECT id,alias FROM tl_page WHERE id=?";
				$objPg = $this->Database->prepare($sql)->execute($apid);
				if ($objPg->numRows) {
					$href = $this->generateFrontendUrl($objPg->row());
				}
			}
		}
		// Start GR 26.04.2014
		$arrDataRef = $this->getReferencesData($this,$catid,$arrSeminar['id'],$arrEvent['id']);
		// start 27.02.2015 GR StartZeit mitgeben
		if (empty($href)) {
			$href = $arrDataRef['href'].'&intStart='.$arrEvent['startTime'].'&intEnd='.$arrEvent['endTime'];//$this->generateFrontendUrl($objEvents->row());
		} else {
			$href = ($arrDataRef['href'] ? $arrDataRef['href'] : $arrDataRef['href_booking']).'&intStart='.$arrEvent['startTime'].'&intEnd='.$arrEvent['endTime'];
		}
		$arrEvent['arrReferences'] = $arrDataRef;
		$jumpToBuchen = $arrDataRef['href_booking'].'&intStart='.$arrEvent['startTime'].'&intEnd='.$arrEvent['endTime'];
		// ende 27.02.2015 GR StartZeit mitgeben
		
		// Ende GR 26.04.2014
		// Start GR 19.04.2014
		//
		if (!empty($arrSeminar['singleSRC'])) {
			$objFile = \FilesModel::findByUuid($arrSeminar['singleSRC']);
			if (!empty($objFile)){
				if (is_file(TL_ROOT . '/' . $objFile->path)) {
					$arrSeminar['src'] = $objFile->path;
				}
			}
		}
		$arrEvent['src'] = $arrSeminar['src'];
		$arrEvent['imageUrl'] = $arrSeminar['src'];
		//
		// Ende GR 19.04.2014
		//
		//
		// Overwrite some settings
		$arrEvent['category'] 			= $cattitle;
		$arrEvent['categoryid'] 		= $catid;
		$arrEvent['href']   			= $href; 
		// Bugfix GR 28.05.2014 - Event-Specials wurde nicht ausgegeben
		$arrEvent['teaser'] 			= $arrSeminar['teaser'];
		$arrEvent['details'] 			= $arrSeminar['details'];
		$arrEvent['specials'] 			= $arrSeminar['specials'];
		//
		$arrEvent['eventTeaser'] 		= $this->String->encodeEmail($objEvents->teaser);
		$arrEvent['eventDetails'] 		= $this->String->encodeEmail($objEvents->details);
		$arrEvent['eventSpecials'] 		= $this->String->encodeEmail($objEvents->specials);
		// Ende GR 28.05.2014
		// Start GR 10.04.2014
		$arrEvent['location'] 			= $arrEvent['location'] ? $arrEvent['location'] :$arrSeminar['location'];
		// Ende GR 10.04.2014
		$arrEvent['duration'] 			= $arrSeminar['duration']; 
		$arrEvent['cost']     			= $arrSeminar['cost'];
		$arrEvent['currency'] 			= $arrSeminar['currency'];  
		$arrEvent['organizer'] 			= $arrSeminar['organizer'];
		$arrEvent['reducedcost'] 		= $arrSeminar['reducedcost']; 
		$arrEvent['sponsorship'] 		= $arrSeminar['sponsorship'];
		$arrEvent['organizergroup'] 	= $arrSeminar['organizergroup'];  
		$arrEvent['facilitatorgroup'] 	= $arrSeminar['facilitatorgroup']; 
		$arrEvent['facilitator']   		= $arrSeminar['facilitator']; 
		//
		$arrEvent['eventOrganizer'] 	= $objEvents->organizer;
		$arrEvent['eventOrganizergroup'] = $objEvents->organizergroup;  
		$arrEvent['eventFacilitatorgroup']	= $objEvents->facilitatorgroup; 
		$arrEvent['eventFacilitator']	= $objEvents->facilitator; 
		$arrEvent['buchen_formular'] 	= $jumpToBuchen;   
		//$arrEvent['places'] 			= $arrSeminar['places'];           
		//$arrEvent['places_min'] 		= $arrSeminar['places_min'];
		$arrDataBk = $this->getBookingState($arrSeminar['places'],$arrSeminar['places_min'],$objEvents->places_booked);
		$arrEvent = array_merge($arrEvent,$arrDataBk);
		$arrEvent['bookstate_class'] 	= $arrDataBk['bookingstate'];
//		$arrEvent['free_places'] 		= $arrDataBk['free_places'];

		$arrEvent['intern'] = $objEvents->intern;
		$arrEvent['date'] = $strDate;
		$arrEvent['time'] = $strTime;
		$arrEvent['startDate'] = $this->parseDate($objPage->dateFormat,$objEvents->date);
		//
		// Start GR 13.02.2014 endDate abfangen
		//
		if (!$objEvents->multipleDate || ($objEvents->endDate <= $objEvents->date)) {
			$strEndDate = null;
		} else {
			$strEndDate = $this->parseDate($objPage->dateFormat, $objEvents->endDate);
		}
		$arrEvent['endDate'] = $strEndDate;
		//
		// Ende GR 13.02.2014 endDate abfangen
		//
		// Start GR 26.04.2014  ursprüngliches Format mit liefern
		$arrEvent['startDate_Raw'] = $objEvents->date;
		$arrEvent['endDate_Raw'] = $objEvents->endDate;
		$arrEvent['startTime_Raw'] = $objEvents->startTime;
		$arrEvent['endTime_Raw'] = $objEvents->endTime;
		// Ende GR 26.04.2014
		$arrEvent['day'] = $strDay;
		$arrEvent['month'] = $strMonth;
		$arrEvent['parent'] = $catid;
		$arrEvent['link'] = $arrSeminar['title'];
		$arrEvent['target'] = '';
		$arrEvent['title'] = specialchars($arrSeminar['title'], true);
		$arrEvent['class'] = ($objEvents->cssClass != '') ? ' ' . $objEvents->cssClass : '';
		//$arrEvent['eventDetails'] = $this->String->encodeEmail($objEvents->details);
		$arrEvent['start'] = $intStart;
		$arrEvent['end'] = $intEnd;

		// Override the link target
		if ($objEvents->source == 'external' && $objEvents->target) {
			$arrEvent['target'] = ($objPage->outputFormat == 'xhtml') ? ' onclick="return !window.open(this.href)"' : ' target="_blank"';
		}

		// Clean the RTE output
		if ($arrEvent['teaser'] != '') {
			if ($objPage->outputFormat == 'xhtml') {
				$arrEvent['teaser'] = $this->String->toXhtml($arrEvent['teaser']);
			} else {
				$arrEvent['teaser'] = $this->String->toHtml5($arrEvent['teaser']);
			}
		}

		// Display the "read more" button for external/article links
		if (($objEvents->source == 'external' || $objEvents->source == 'article') && $objEvents->details == '') {
			$arrEvent['eventDetails'] = true;
		} else {
			// Clean the RTE output
			if ($objPage->outputFormat == 'xhtml') {
				$arrEvent['teaser'] = $this->String->toXhtml($arrEvent['teaser']);
				$arrEvent['details'] = $this->String->toXhtml($arrEvent['details']);
				$arrEvent['specials'] = $this->String->toXhtml($arrEvent['specials']);
				$arrEvent['eventSpecials'] = $this->String->toXhtml($arrEvent['eventSpecials']);
				$arrEvent['eventDetails'] = $this->String->toXhtml($arrEvent['eventDetails']);
			} else {
				$arrEvent['teaser'] = $this->String->toHtml5($arrEvent['teaser']);
				$arrEvent['details'] = $this->String->toHtml5($arrEvent['details']);
				$arrEvent['specials'] = $this->String->toHtml5($arrEvent['specials']);
				$arrEvent['eventSpecials'] = $this->String->toHtml5($arrEvent['eventSpecials']);
				$arrEvent['eventDetails'] = $this->String->toHtml5($arrEvent['eventDetails']);
			}
		}

		// Get todays start and end timestamp
		if ($this->intTodayBegin === null) {
			$this->intTodayBegin = strtotime('00:00:00');
		}
		if ($this->intTodayEnd === null) {
			$this->intTodayEnd = strtotime('23:59:59');
		}

		// Mark past and upcoming events (see #3692)
		if ($intEnd < $this->intTodayBegin)	{
			$arrEvent['class'] .= ' bygone';
		} elseif ($intStart > $this->intTodayEnd) {
			$arrEvent['class'] .= ' upcoming';
		} else {
			$arrEvent['class'] .= ' current';
		}

		if ($objEvent->recurring) {
			$arrList = $this->getRecurringEvents($objEvents,$time,$objEvents->endDate);
			$arrEvent['arrRecurring'] = $arrList;
		}
		$arrDataICAL = $this->getICALEvent($arrEvent,$arrSeminar);	
		$arrEvent = array_merge($arrEvent,$arrDataICAL);
		$eventid = $arrEvent['id'];
		if (empty($GLOBALS['ICAL'][$eventid])) {
			$GLOBALS['ICAL'][$eventid] = $arrDataICAL;			
		}

		$this->arrEvents[$intKey][$intStart][] = $arrEvent;

	}


	/**
	 * Generate a URL and return it as string
	 * @param Database_Result
	 * @param string
	 * @return string
	 */
	protected function generateEventUrl(Database_Result $objEvent, $strUrl)
	{
		switch ($objEvent->source) {
			// Link to an external page
			case 'external':
				$this->import('String');

				if (substr($objEvent->url, 0, 7) == 'mailto:') {
					return $this->String->encodeEmail($objEvent->url);
				} else {
					return ampersand($objEvent->url);
				}
				break;

			// Link to an internal page
			case 'internal':
				$objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
									 	  ->limit(1)
										  ->execute($objEvent->sv_jumpTo);

				if ($objPage->numRows) {
					return ampersand($this->generateFrontendUrl($objPage->row()));
				}
				break;

			// Link to an article
			case 'article':
				$objPage = $this->Database->prepare("SELECT a.id AS aId, a.alias AS aAlias, a.title, p.id, p.alias FROM tl_article a, tl_page p WHERE a.pid=p.id AND a.id=?")
										  ->limit(1)
										  ->execute($objEvent->articleId);

				if ($objPage->numRows) {
					return ampersand($this->generateFrontendUrl($objPage->row(), '/articles/' . ((!$GLOBALS['TL_CONFIG']['disableAlias'] && $objPage->aAlias != '') ? $objPage->aAlias : $objPage->aId)));
				}
				break;
		}

		// Link to the default page
		return ampersand(sprintf($strUrl, ((!$GLOBALS['TL_CONFIG']['disableAlias'] && $objEvent->alias != '') ? $objEvent->alias : $objEvent->id)));
	}

	//
	// Ermitteln der Seminarwiederholungen
	//
	private function getRecurringEvents($objEvent, $intStart, $intEnd) {
		global $objPage;
		$arrRecurring = array();
		// Recurring events
		if ($objEvent->recurring)
		{
			$count = 0;

			$arrRepeat = deserialize($objEvent->repeatEach);
			$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];

			if ($arrRepeat['value'] >= 1) {
				if ($objEvent->recurrences > 0) {
					while ($objEvent->endTime < $intEnd) {
						$count++;
						if ($count >= $objEvent->recurrences) {
							break;
						}
		
						$objEvent->startTime = strtotime($strtotime, $objEvent->startTime);
						$objEvent->endTime = strtotime($strtotime, $objEvent->endTime);
		
						// Skip events outside the scope
						if ($objEvent->endTime < $intStart || $objEvent->startTime > $intEnd) {
							continue;
						}
		
						$strRecurring = $this->parseDate($objPage->dateFormat, $objEvent->startTime);
						$arrRecurring[$count-1]['date'] = $strRecurring;
						$arrRecurring[$count-1]['dateRaw'] = $objEvent->startTime;
						$arrRecurring[$count-1]['count'] = $count-1;
					}
				}
			}
		}
		return $arrRecurring;
	}


	/**
	 * Return the begin and end timestamp and an error message as array
	 * @param Date
	 * @param string
	 * @return array
	 */
	protected function getDatesFromFormat(Date $objDate, $strFormat) {
		switch ($strFormat)	{
			case 'cal_day':
			case 'sv_day':
				return array($objDate->dayBegin, $objDate->dayEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyDay']);
				break;

			default:
			case 'cal_month':
			case 'sv_month':
				return array($objDate->monthBegin, $objDate->monthEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyMonth']);
				break;

			case 'cal_year':
			case 'sv_year':
				return array($objDate->yearBegin, $objDate->yearEnd, $GLOBALS['TL_LANG']['MSC']['cal_emptyYear']);
				break;

			case 'cal_all': // 1970-01-01 00:00:00 - 2038-01-01 00:00:00
			case 'sv_all': // 1970-01-01 00:00:00 - 2038-01-01 00:00:00
				return array(0, 2145913200, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_7':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+7 days', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_14':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+14 days', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_30':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+1 month', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_90':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+3 months', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_180':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+6 months', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_365':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+1 year', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_two':
				$objToday = new Date();
				return array($objToday->dayBegin, (strtotime('+2 years', $objToday->dayBegin) - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_cur_month':
				$objToday = new Date();
				return array($objToday->dayBegin, $objToday->monthEnd, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_cur_year':
				$objToday = new Date();
				return array($objToday->dayBegin, $objToday->yearEnd, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'next_all': // 2038-01-01 00:00:00
				$objToday = new Date();
				return array($objToday->dayBegin, 2145913200, $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_7':
				$objToday = new Date();
				return array((strtotime('-7 days', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_14':
				$objToday = new Date();
				return array((strtotime('-14 days', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_30':
				$objToday = new Date();
				return array((strtotime('-1 month', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_90':
				$objToday = new Date();
				return array((strtotime('-3 months', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_180':
				$objToday = new Date();
				return array((strtotime('-6 months', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_365':
				$objToday = new Date();
				return array((strtotime('-1 year', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_two':
				$objToday = new Date();
				return array((strtotime('-2 years', $objToday->dayBegin) - 1), ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_cur_month':
				$objToday = new Date();
				return array($objToday->monthBegin, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_cur_year':
				$objToday = new Date();
				return array($objToday->yearBegin, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;

			case 'past_all': // 1970-01-01 00:00:00
				$objToday = new Date();
				return array(0, ($objToday->dayBegin - 1), $GLOBALS['TL_LANG']['MSC']['cal_empty']);
				break;
		}
	}
	/**
	 * Calculate the span between two timestamps in days
	 * @param integer
	 * @param integer
	 * @return integer
	 */
	public static function calculateSpan($intStart, $intEnd)
	{
		return self::unixToJd($intEnd) - self::unixToJd($intStart);
	}


	/**
	 * Convert a UNIX timestamp to a Julian day
	 * @param integer
	 * @return integer
	 */
	public static function unixToJd($tstamp)
	{
		list($year, $month, $day) = explode(',', date('Y,m,d', $tstamp));

		// Make year a positive number
		$year += ($year < 0 ? 4801 : 4800);

		// Adjust the start of the year
		if ($month > 2)
		{
			$month -= 3;
		}
		else
		{
			$month += 9;
			--$year;
		}

		$sdn  = floor((floor($year / 100) * 146097) / 4);
		$sdn += floor((($year % 100) * 1461) / 4);
		$sdn += floor(($month * 153 + 2) / 5);
		$sdn += $day - 32045;

		return $sdn;
	}

	public function getAllLocation ($arrCat, $fltLocation = '', $fltCategory=0, $seminarid=0, $eventid=0, $intStart=0, $intEnd=9999999999) {
		global $objPage;
		global $arrBookings;
	    //
	    $time = time();
	    $arrLocation = array();
	    $arrLocations = array();
	    $arrLocationList = array();
	    $arrCategories = array();
	    $arrSeminars = array();
	    $arrEvents = array();
    	$arrData = array();
	    foreach ($arrCat as $category) {
	    	//
	    	// get category data
	    	//
			$sqlCat = "SELECT * FROM tl_seminar_category WHERE id=?";
			$objCategory = $this->Database->prepare($sqlCat)->limit(1)->execute($category);
			if ($objCategory->numRows) {
				$arrSeminar = array();
				$arrCategory = $objCategory->row();
				$catname = $objCategory->title;
				$ckey = $objCategory->id;
				$arrCategories[$ckey] = $arrCategory;
		    	//
		    	// get seminar data
		    	//
				if ($seminarid === 0) {
					$sqlSem = "SELECT * FROM tl_seminar WHERE pid=?";
					$sid = $category;
				} else {
					$sqlSem = "SELECT * FROM tl_seminar WHERE id=?";
					$sid = $seminarid;
				}
				$objSeminar = $this->Database->prepare($sqlSem)->execute($sid);
				while ($objSeminar->next()) {
					$semid   = $objSeminar->id;
					$semname = $objSeminar->title;
					$arrSeminar = $objSeminar->row();
					$arrSeminar['location'] = trim($arrSeminar['location']);
			    	//
			    	// get event data
			    	//
					if ($eventid === 0) {
						//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND date>$time ORDER BY date";
						$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((startTime>=? AND startTime<=?) OR (endTime>=? AND endTime<=?) OR (startTime<=? AND endTime>=?) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=?) AND startTime<=?))" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . " ORDER BY startTime";
						//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? ORDER BY date";
						$id = $semid;
					} else {
						//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE id=? AND date>$time ORDER BY date";
						$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((startTime>=? AND startTime<=?) OR (endTime>=? AND endTime<=?) OR (startTime<=? AND endTime>=?) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=?) AND startTime<=?))" . (!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . " ORDER BY startTime";
						//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE id=? ORDER BY date";
						$id = $eventid;
					}
					$skey = $semid;
					$arrSeminars[$skey] = $arrSeminar;
					$objEvent = $this->Database->prepare($sqlEvt)->execute($id,$intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd, $intStart, $intEnd);
					while ($objEvent->next()) {
						$arrEvent = array();
						$location = trim($objEvent->location);
						$location = $location ? $location : $arrSeminar['location'];
						if (!empty($location) && (($fltLocation == $location) || ($fltLocation == '*'))) {
							$evtid   = $objEvent->id;
							$evtintern = $objEvent->intern;
							$evtaddTime = $objEvent->addTime;
							$evtmultipleDate = $objEvent->multipleDate;
							$evtdate = $objEvent->date;
							$evtendDate = $objEvent->endDate;
							$evtstartTime = $objEvent->startTime;
							$evtendTime = $objEvent->endTime;
							$arrEvent = $objEvent->row();
							$arrEvent['arrSeminar'] = $arrSeminar;
							$arrEvent['category'] = $catname;
							$arrEvent['seminar'] = $semname;
							$arrEvent['event_intern'] = $evtintern;
							//$arrEvent['addTime'] = $evtaddTime;
							//$arrEvent['multipleDate'] = $evtmultipleDate;
							$arrEvent['startDate'] = $this->parseDate($objPage->dateFormat,$evtdate);
							$arrEvent['endDate'] = $this->parseDate($objPage->dateFormat,$evtendDate);
							$arrEvent['startTime'] = $this->parseDate($objPage->timeFormat,$evtstartTime);
							$arrEvent['endTime'] = $this->parseDate($objPage->timeFormat,$evtendTime);
							$arrEvent['startDate_Raw'] = $evtdate;
							$arrEvent['endDate_Raw'] = $evtendDate;
							$arrEvent['startTime_Raw'] = $evtstartTime;
							$arrEvent['endTime_Raw'] = $evtendTime;
							//
							//
							// Get Category/Seminar/Module sv_jumpTo
							//
							$arrDataReferent = $this->getReferentData($arrSeminar['facilitator']);
							$arrDataReferences = $this->getReferencesData($this,$objSeminar->pid,$objSeminar->id,$objEvent->id);
							$arrDataBooking = $this->getBookingState($objSeminar->places,$objSeminar->places_min,$objEvent->places_booked);
							$arrDataICAL = $this->getICALEvent($arrEvent,$arrSeminar);	
							$arrEvent = array_merge($arrDataReferent,$arrEvent);
							$arrEvent = array_merge($arrDataReferences,$arrEvent);
							$arrEvent = array_merge($arrDataBooking,$arrEvent);
							$arrEvent = array_merge($arrDataICAL,$arrEvent);

							$GLOBALS['ICAL'][$evtid] = $arrDataICAL;			
							//
							// key für Data und Locations: <location>_<event_time>
							//
							$strLocation =$arrEvent['location'] ? $arrEvent['location'] : $arrSeminar['location'];
							$tm = $this->parseDate("Ymd",$objEvent->date).$this->parseDate("His",$objEvent->startTime);
							//$key = $strLocation.'_'.$tm;
							/* GR 17.12.2014 Key eindeutig mit EVENT ID versehen */
							$key = $tm. '_' .$strLocation.'_'.$evtid;
							//
							$arrEvent['location'] = $strLocation;
							/* GR 17.12.2014 Key eindeutig mit EVENT ID versehen */
							$ekey = $tm.'_'.$evtid;
							if (!empty($fltLocation) && (($fltLocation == $strLocation) || ($fltLocation == '*'))) {
								$arrEvents[$ekey] = $arrEvent;
								$arrLocation['location'] = $strLocation;
								$arrLocation['event_id'] = $objEvent->id;
								$arrLocation['seminar_id'] = $objSeminar->id;
								$arrLocation['category_id'] = $objCategory->id;
								$arrLocations[$key] = $arrLocation;
								if (!in_array($strLocation, $arrLocationList)) {
									$arrLocationList[] = $strLocation;
								}
							}
							$arrData[$key] = $arrEvent;
						}
					}
				}
			}
		}
		//
		// Sortierungen
		ksort($arrLocations);
		ksort($arrLocationList);
		ksort($arrEvents);
		ksort($arrData);
		// verfügbar machen
		$arrData['arrLocationList'] = $arrLocationList;
		$arrData['arrLocations'] = $arrLocations;
		$arrData['arrCategories'] = $arrCategories;
		$arrData['arrSeminars'] = $arrSeminars;
		$arrData['arrEvents'] = $arrEvents;
		
		return $arrData;
	}

	/* Start GR 25.04.2014 - verschoben wg. der Referenten-Info */
	//
	// getReferentData - Datensatz der Referenten/Member zu einem Seminar zurückgeben
	//
	public function getReferentData($memberId) {
		
		$arrData = array();
		// Defaults
		$facilitatorName = '';
		$arrData['arrReferent'] = array();									
		$arrData['referent'] = $facilitatorName;	
	    // Referent(in)
		$sqlF = "SELECT * FROM tl_member WHERE id=?";
		$objMember = $this->Database->prepare($sqlF)->execute($memberId);
		if ($objMember->numRows) {
			$facilitatorName = $objMember->lastname.', '.$objMember->firstname; // Kompatibilität
			$arrData['arrReferent'] = $objMember->row();
			$arrData['referent'] = $facilitatorName;	
		}     
		return $arrData;
	}

	//
	// getCategoryData - Datensatz der Category zu einem Seminar zurückgeben
	//
	public function getCategoryData($catid) {
		
		$arrData = array();
		$sql = "SELECT * FROM tl_seminar_category WHERE id=?";
		$objCategory = $this->Database->prepare($sql)->execute($catid);
		if ($objCategory->numRows) {
			$arrData = $objCategory->row();
		}
		return $arrData;
	}
	//
	// getReaderPage - URL zur Zielseite ermitteln (indirekt und direkt)
	// .. direkt erfolgt wenn $strTable leer ist
	//
	public function getReaderPage($id,$strTable,$strJumpTo) {
		// return default is empty
		$jt = '';
		// Page Select sql
	    $sql = "SELECT id, alias FROM tl_page WHERE id=?";
		// direct Page Selection
		if (empty($strTable)) {
		    $objJump = $this->Database->prepare($sql)
			    	->execute($id);
		    if ($objJump->numRows) {
		    	$jt = $this->generateFrontendUrl($objJump->row());
		    }
		// get jumpTo column first
		} else {
			$objTable = $this->Database->prepare("SELECT * FROM ".$strTable." WHERE id=?")
			->execute($id);
			if ($objTable->numRows) {
				$tarPage   = $objTable->$strJumpTo;
				//  Page Selection
			    $objJump = $this->Database->prepare($sql)
				    	->execute($tarPage);
			    if ($objJump->numRows) {
			    	$jt = $this->generateFrontendUrl($objJump->row());
			    }
			}
		}
		return $jt;
	}

	/*
	 * getReferencesData - Refernezen zu Zielseiten ermitteln
	 *                     Reihenfolge: Modul -> Seminar -> Category -> aktuelle Seite
	 *
	 */
	public function getReferencesData ($objModule,$intCatId=0,$intSeminarId=0,$intEventId=0) {
		//
		// Get Category jumpTo
		//
		$catJumpTo = '';
		$catJumpTo = $this->getReaderPage($intCatId,'tl_seminar_category','sv_jumpTo_category');
		//
		// Get Seminar jumpTo
		//
		$semJumpTo = '';
		$semJumpTo = $this->getReaderPage($intSeminarId,'tl_seminar','sv_jumpTo');
		//
		// Get Event jumpTo
		//
		$evtJumpTo = '';
		//$evtJumpTo = $this->getReaderPage($intEventId,'tl_seminar_events','sv_jumpTo');
		//
		// Get Booking jumpToBooking reference
		//
		$bookJumpTo = '';
		if (!empty($objModule->sv_jumpToBuchen)) {
			$bookJumpTo = $this->getReaderPage($objModule->sv_jumpToBuchen,NULL,NULL);
			if (empty($bookJumpTo)) {
				$bookJumpTo = $this->getReaderPage($intSeminarId,'tl_seminar','jumpToBooking');
			}
		}
		// Module/ContentElement jumpTo
		$cteJumpTo = '';
		if (!empty($objModule->sv_jumpTo)) {
			$cteJumpTo = $this->getReaderPage($objModule->sv_jumpTo,NULL,NULL);
		}	
		// get last jumpTo od hierarchie
		if (!empty($cteJumpTo)) {
			$jumpTo = $cteJumpTo;
		} else if (!empty($semJumpTo)) {
		    $jumpTo = $semJumpTo;
		} else if (!empty($catJumpTo)) {
			$jumpTo = $catJumpTo;
		} else {
			$jumpTo = $this->Environment->requestUri;
		}
		//
		// Seminardaten ergänzen: href, category, seminar, event
		//
		$parms = '';
		if (!empty($intCatId) && $intCatId > 0) {
			$parms .= (empty($parms) ? '?' : '&') .'category='.$intCatId;
		}
		if (!empty($intSeminarId) && $intSeminarId > 0) {
			$parms .= (empty($parms) ? '?' : '&') .'seminar='.$intSeminarId;
		}
		if (!empty($intEventId) && $intEventId > 0) {
			$parms .= (empty($parms) ? '?' : '&') .'event='.$intEventId;
		}
		//
		// Referenzliste aufbauen 
		//
		$arrData = array();
		if (!empty($jumpTo)) {
			$arrData['href'] = $jumpTo.$parms;
		} else {
			$arrData['href'] = '';
		}
		if (!empty($bookJumpTo)) {
			$arrData['href_booking'] = $bookJumpTo.$parms;
		} else {
			$arrData['href_booking'] = '';
		}
		if (!empty($catJumpTo)) {
			$arrData['href_category'] = $catJumpTo.$parms;
		} else {
			$arrData['href_category'] = '';
		}
		if (!empty($semJumpTo)) {
			$arrData['href_seminar'] = $semJumpTo.$parms;
		} else if (!empty($jumpTo) && $intSeminarId > 0) {
			$arrData['href_seminar'] = $jumpTo.$parms;
		} else {
			$arrData['href_seminar'] = '';
		}
		if (!empty($jumpTo) && $intEventId > 0) {
			$arrData['href_event'] = $jumpTo.$parms;
		} else {
			$arrData['href_event'] = '';
		}
		return $arrData;
	}

	/*
	 * getBookingState - Buchungsstatus (AmpeL: green, yellow, red) und freie Plätze ermitteln
	 *
	 */
	public function getBookingState($places,$places_min,$places_booked) {
		$bookstate = 'green';
		$maxPlaces = $places;
		$minPlaces = $places_min;
		$bookedPlaces = $places_booked;
		$freePlaces = $maxPlaces - $bookedPlaces;
		if ($freePlaces < 0) $freePlaces = 0;
		if ($freePlaces == 0) {
			$bookstate = 'red';
		} else if ($bookedPlaces >= $minPlaces) {
			$bookstate = 'yellow';
		}
		$arrData = array();
		$arrData['min_places'] = $minPlaces;
		$arrData['max_places'] = $maxPlaces;
		$arrData['booked_places'] = $bookedPlaces;
		$arrData['free_places'] = $freePlaces;
		$arrData['bookingstate'] = $bookstate;
		return $arrData;
	}	

	public function getICALEvent($arrEvent,$arrSeminar) {
		$arrData = array();
		
		if (in_array('ical_creator', $this->Config->getActiveModules())) {

			$ical = new \vcalendar();
			$ical->setConfig('ical_' . $arrEvent['id'], 'wdi.de' );
			$ical->setProperty( 'method', 'PUBLISH' );
			$ical->setProperty( "X-WR-CALNAME", (strlen($arrEvent['intern'])) ? $arrEvent['intern'] : $arrSeminar['intern']);
			$ical->setProperty( "X-WR-CALDESC", $arrSeminar['title']);
			$ical->setProperty( "X-WR-TIMEZONE", $GLOBALS['TL_CONFIG']['timeZone']);

			//
			$vevent = new \vevent();
			if ($arrEvent['addTime'])
			{
				$vevent->setProperty( 'dtstart', array( 'year'=>date('Y', $arrEvent['startTime_Raw']), 'month'=>date('m', $arrEvent['startTime_Raw']), 'day'=>date('d', $arrEvent['startTime_Raw']), 'hour'=>date('H', $arrEvent['startTime_Raw']), 'min'=>date('i', $arrEvent['startTime_Raw']),  'sec'=>0 ));
				$vevent->setProperty( 'dtend', array( 'year'=>date('Y', $arrEvent['endTime_Raw']), 'month'=>date('m', $arrEvent['endTime_Raw']), 'day'=>date('d', $arrEvent['endTime_Raw']), 'hour'=>date('H', $arrEvent['endTime_Raw']), 'min'=>date('i', $arrEvent['endTime_Raw']),  'sec'=>0 ));
			}
			else
			{
				$vevent->setProperty( 'dtstart', date('Ymd', $arrEvent['date_Raw']), array('VALUE' => 'DATE'));
				if ($arrEvent['multipleDates'] || !strlen($arrEvent['endDate_Raw']) || $arrEvent['endDate_Raw'] == 0)
				{
					$vevent->setProperty( 'dtend', date('Ymd', $arrEvent['date_Raw'] + 24*60*60), array('VALUE' => 'DATE'));
				}
				else
				{
					$vevent->setProperty( 'dtend', date('Ymd', $arrEvent['endDate_Raw'] + 24*60*60), array('VALUE' => 'DATE'));
				}
			}

			$icalPrefix = $GLOBALS['TL_CONFIG']['websiteTitle'];
			
			$vevent->setProperty( 'summary', html_entity_decode((strlen($icalPrefix) ? $icalPrefix . " " : "") . $arrEvent['intern'], ENT_QUOTES, 'UTF-8'));
			$vevent->setProperty( 'description', html_entity_decode(strip_tags(preg_replace('/<br \\/>/', "\n", $arrSeminar['details'])), ENT_QUOTES, 'UTF-8'));
			if ($arrEvent['recurring'])
			{
				$count = 0;
				$arrRepeat = deserialize($arrEvent['repeatEach']);
				$intRepeat = $arrRepeat['value'];
				$strRepeatStep = $arrRepeat['unit'];
				if ($intRepeat == 1)
				{
					$strRepeatStep = substr($strRepeatStep, 0, -1);
				}
	
				$frequence = 'YEARLY';
				switch ($strRepeatStep)
				{
					case 'days':
						$frequence = 'DAILY';
						break;
					case 'weeks':
						$frequence = 'WEEKLY';
						break;
					case 'months':
						$frequence = 'MONTHLY';
						break;
					case 'years':
						$frequence = 'YEARLY';
						break;
				}
				$rrule = array('FREQ' => $frequence);
				if ($arrEvent['recurrences'] > 0)
				{
					$rrule['count'] = $arrEvent['recurrences'];
				}
				if ($intRepeat > 1)
				{
					$rrule['INTERVAL'] = $intRepeat;
				}
				$vevent->setProperty( 'rrule', $rrule);
			}
	
			$ical->setComponent ($vevent);
			$arrIcal['ical'] = $ical;
			$arrIcal['href_ical'] = $this->addToUrl("ical=".$arrEvent['id']."&title=".$arrEvent['intern']);
			$arrIcal['title_ical'] = $arrEvent['intern'];
			$arrIcal['link_ical'] = $arrEvent['intern'];
	
			$arrData['icalname'] = $arrEvent['id'];
			$arrData['icaldata'] = $arrIcal;
		}
		//
		return $arrData;
	}
	
}

?>