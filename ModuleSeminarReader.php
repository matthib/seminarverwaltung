<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2014
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
 * Class ModuleSeminarReader
 *
 * Front end module "seminar reader".
 * @copyright  Gerd Regnery 2011
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 */
class ModuleSeminarReader extends SeminarManager
{

	/**
	 * Current date object
	 * @var integer
	 */
	protected $Date;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_seminarreader';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### SEMINAR READER ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		// Return if no event has been specified
		if (!$this->Input->get('seminar'))
		{
			global $objPage;

			// Do not index the page
			$objPage->noSearch = 1;
			$objPage->cache = 0;

			return '';
		} 
		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		global $objPage;

		$this->Template->seminar = '';
		$this->Template->referer = 'javascript:history.go(-1)';
		$this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

		$time = time(); 
		$id = is_numeric($this->Input->get('seminar'))?$this->Input->get('seminar'):0;
		$evtId = is_numeric($this->Input->get('event'))?$this->Input->get('event'):0;
		$intStart = is_numeric($this->Input->get('intStart'))?$this->Input->get('intStart'):0;
		$intEnd = is_numeric($this->Input->get('intEnd'))?$this->Input->get('intEnd'):0;
        $this->Session->set('seminaridx',$id);
        $this->Session->set('eventidx',$evtId);
        $this->Session->set('intStart',$intStart);
        $this->Session->set('intEnd',$intEnd);

		// Get all seminar events
		$sql = "SELECT * FROM tl_seminar WHERE id=?";
		$result = $this->Database->prepare($sql)
									 ->execute($id);
		$strSeminars = '';		
		$arr = Array();								
		if ($result->numRows) {
			while ($result->next()) {
				$row = $result->row();
				foreach ($row as $key => $val) {
					$arr[$key] = $val;
				}
			}
			$arr['arrCategory'] = $this->getCategoryData($result->pid); //GR 25.04.2014
		}

 	    $arrData = array(); 
		$objTemplate = new FrontendTemplate($this->sv_stemplate);

		$sql = "SELECT * FROM tl_seminar_events WHERE pid=? AND id=? AND published=1";
		$objEvents = $this->Database->prepare($sql)
				->limit(1)->execute($id,$evtId);
		$arrDate = Array();
	    if ($objEvents->numRows) {
	    	// Startdatum
	      $intStart = ($intStart ? $intStart : $objEvents->startTime);
		  $intEnd = ($intEnd ? $intEnd : $objEvents->endTime);
		  // start 27.02.2015 GR
		  $curStartTime = $intStart;
		  $curEndTime = $intEnd;
		  // ende 27.02.2015 GR
	      $startTime = $objEvents->startTime; //$objEvents->startTime;
	      $endTime = $objEvents->endTime;
	      $intEnd = $endTime;
	      $intStart = $startTime;
	      $published = $objEvents->published;
	      $stop = $startDate + (24*60*60);
		  // alle  Termine
		  // Datum und Zeitformate ermitteln
		  $strDate = $this->parseDate($objPage->dateFormat,$startTime);

		  $daySec = 24*60*60; // Tag in Sekunden
		  $intStartDays = floor($startTime / $daySec); // Stardatum in Tagen
		  $intEndDays = floor($endTime / $daySec);     // Enddatum in Tagen
		  $diff = $intEndDays - $intStartDays;
		  if ($diff > 0) {
			$strDate = $this->parseDate($objPage->dateFormat, $startTime) . ' - ' . $this->parseDate($objPage->dateFormat, $endTime);
		  }

		  $strTime = '';

		  if ($objEvents->addTime) {
		    if (($endTime > $startTime) && ($diff > 0)) {
				$strDate = $this->parseDate($objPage->datimFormat, $startTime) . ' - ' . $this->parseDate($objPage->datimFormat, $endTime);
		    } else if ($startTime == $endTime) {
				$strTime = $this->parseDate($objPage->timeFormat, $startTime);
			} else {
				$strTime = $this->parseDate($objPage->timeFormat, $startTime) . ' - ' . $this->parseDate($objPage->timeFormat, $endTime);
			}
		  }
		  $row = $objEvents->row();
	      $arrDate = $row;
	      unset($arrDate['details']);
	      unset($arrDate['specials']);
	      $arrDate['diff'] = $diff;
		  $arrDate['daySec'] = $daySec;
		  $arrDate['intStartD'] = $intStartDays;
		  $arrDate['intEndD'] = $intEndDays;
		  $arrDate['date'] = $strDate;
		  $arrDate['time'] = $strTime;
		  // start 27.02.2015 GR
		  $arrDate['curStartTime'] = $curStartTime;
		  $arrDate['curEndTime'] = $curEndTime;
		  // ende 27.02.2015 GR
		  $arrDate['startDate_Raw'] = $objEvents->date;
		  $arrDate['startTime_Raw'] = $objEvents->startTime;
		  $arrDate['endDate_Raw'] = $objEvents->endDate;
		  $arrDate['endTime_Raw'] = $objEvents->endTime;
		  
	      $arrDate['startDate'] = $this->parseDate($objPage->dateFormat,$objEvents->date);
	      if ($objEvents->endDate == $objEvents->date) {
	      	$arrDate['endDate'] = '';
	      } else {
	      	$arrDate['endDate'] = $this->parseDate($objPage->dateFormat,$objEvents->endDate);
	      }
	      $arrDate['evtDetails'] = $objEvents->details;
	      $arrDate['evtSpecials'] = $objEvents->specials;
	      $arrDate['last_request_days'] = $objEvents->last_request_days;
	      $arrDate['last_request_direction'] = $objEvents->last_request_direction;
	      $arrData = $this->getReferencesData($this,$result->pid,$result->id,$objEvents->id);
	      $arrDate = array_merge($arrDate,$arrData);
	    } else {
	      $arrDate = $this->getReferencesData($this,$result->pid,$result->id,0);
	    }
    	$arr['buchen_formular'] = $arrDate['href_booking'] ? ($arrDate['href_booking'].'&intStart='.$intStart.'&intEnd='.$intEnd) : '';

	    // Referent(in)
		/* Start GR 25.04.2014 - verschoben wg. der Referenten-Info */
		$arrData = $this->getReferentData($result->facilitator);
		$facilitatorName = $arrData['referent'];
		$arrDate['arrReferent'] = $arrData['arrReferent'];
		$arrDataBooking = $this->getBookingState($arr['places'],$arr['places_min'],$objEvents->places_booked);
		$arrDataICAL = $this->getICALEvent($arrDate,$arr);	
		$arrDate = array_merge($arrDataBooking,$arrDate);
		$arrDate = array_merge($arrDataICAL,$arrDate);

		$GLOBALS['ICAL'][$evtId] = $arrDataICAL;	
		
		if (empty($arrDate['location'])) {
			$arrDate['location'] = $arr['location'];
		}
				
		//
	    if (empty($arrDate)) {
	    	$objTemplate->setData($arr);
	    } else {		
	    	$objTemplate->setData(array_merge($arr,$arrDate));
	    }
		// Start GR 19.04.2014
		//
		if ($arr['addImage'] && !empty($arr['singleSRC'])) {
			$objFile = \FilesModel::findByUuid($arr['singleSRC']);
			if ($objFile === null)
			{
				if (!\Validator::isUuid($objEvent->singleSRC))
				{
					$objTemplate->text = '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
				}
			}
			elseif (is_file(TL_ROOT . '/' . $objFile->path))
			{
				$arr['src'] = $objFile->path;
				$arr['singleSRC'] = $objFile->path;
				$this->addImageToTemplate($objTemplate, $arr);
			}
		}
		/* Ende GR 25.04.2014 */

	   	$objTemplate->referent = $facilitatorName;
	    $objTemplate->id=$id;
	    $objTemplate->published = $published;
	    $objTemplate->stop = $stop;
		$objTemplate->enclosure = array();

		// Add enclosures
		if ($result->addEnclosure)
		{
			$this->addEnclosuresToTemplate($objTemplate, $result->row());
		}

		$this->Template->seminar = $objTemplate->parse();

	}
	
}

?>