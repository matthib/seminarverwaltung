<?php

class searchPages extends Frontend {
	
		/**
	 * Add events to the indexer
	 * @param array
	 * @param integer
	 * @param boolean
	 * @return array
	 */
	public function getSearchablePages($arrPages, $intRoot=0, $blnIsSitemap=false)
	{
		$arrRoot = array();

		if ($intRoot > 0)
		{
			$arrRoot = $this->Database->getChildRecords($intRoot, 'tl_page');
		}

		$time = time();
		$arrProcessed = array();

		// Get all calendars
		$objCategory = \SeminarCategoryModel::findByProtected('');

		// Walk through each category
		if ($objCategory !== null)
		{
			while ($objCategory->next())
			{
				// Skip calendars without target page
				if (!$objCategory->sv_jumpTo)
				{
					continue;
				}

				// Skip calendars outside the root nodes
				if (!empty($arrRoot) && !in_array($objCategory->sv_jumpTo, $arrRoot))
				{
					continue;
				}

				// Get the URL of the sv_jumpTo page
				if (!isset($arrProcessed[$objCategory->sv_jumpTo]))
				{
					$objParent = \PageModel::findWithDetails($objCategory->sv_jumpTo);

					// The target page does not exist
					if ($objParent === null)
					{
						continue;
					}

					// The target page has not been published (see #5520)
					if (!$objParent->published || ($objParent->start != '' && $objParent->start > $time) || ($objParent->stop != '' && $objParent->stop < $time))
					{
						continue;
					}

					// The target page is exempt from the sitemap (see #6418)
					if ($blnIsSitemap && $objParent->sitemap == 'map_never')
					{
						continue;
					}

					// Set the domain (see #6421)
					$domain = ($objParent->rootUseSSL ? 'https://' : 'http://') . ($objParent->domain ?: \Environment::get('host')) . TL_PATH . '/';

					// Generate the URL
					$arrProcessed[$objCategory->sv_jumpTo] = $domain . $this->generateFrontendUrl($objParent->row(), (($GLOBALS['TL_CONFIG']['useAutoItem'] && !$GLOBALS['TL_CONFIG']['disableAlias']) ?  '.html?category=%s&seminar=%s&event=%s' : '?category=%s&seminar=%s&event=%s'), $objParent->language);
				}

				$strUrl = $arrProcessed[$objCategory->sv_jumpTo];
				
				// Get the seminars
				
				$objSeminars = \SeminarModel::findPublishedDefaultByPid($objCategory->id);

				// Walk through each category
				if ($objSeminars !== null)
				{
					while ($objSeminars->next())
					{
/* 						$arrPages[] = sprintf($strUrl, (($objSeminars->alias != '' && !$GLOBALS['TL_CONFIG']['disableAlias']) ? $objSeminars->alias : $objSeminars->id)); */
						// Get the items
						$objEvents = \SeminarEventsModel::findPublishedDefaultByPid($objSeminars->id);
		
						if ($objEvents !== null)
						{
							while ($objEvents->next())
							{
								$arrPages[] = sprintf($strUrl, 
								(($objCategory->title != '' && 
								!$GLOBALS['TL_CONFIG']['disableAlias']) ? $objSeminars->id : $objCategory->id), 
								(($objSeminars->intern != '' && 
								!$GLOBALS['TL_CONFIG']['disableAlias']) ? $objSeminars->id : $objCategory->id), 
								(($objEvents->intern != '' && 
								!$GLOBALS['TL_CONFIG']['disableAlias']) ? $objEvents->id : $objEvents->id) );    //, 
/* 								(($objEvents->date != '' && !$GLOBALS['TL_CONFIG']['disableAlias']) ? \Date("Y-m-d",$objEvents->date) : $objEvents->id)); */
							}
						}
					}
/* 					echo '<pre>';print_r($arrPages); echo '</pre>'; */
				}
					
			}
		}

		return $arrPages;
	}


}


?>