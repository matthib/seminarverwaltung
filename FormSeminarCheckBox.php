<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

class FormSeminarCheckBox extends FormSelectMenu {
	
	protected $arrData;
	
	public function __construct() {
		parent::__construct();
		$time = time();
		$this->arrData = array();
		$this->Import('Database');
		$sqlCat = "SELECT * FROM tl_seminar_category ORDER BY title ASC";
		$objCat = $this->Database->prepare($sqlCat)->execute();
		$i = 0;
		$arrCat = array();
		$arrCat[0] = '---';
	    while ($objCat->next()) {
	    	$catid    = $objCat->id;
	    	$cattitle = $objCat->title;
	    	$i += 1; 
	 			$arrCat[$catid] = $cattitle;
	    }
		$this->arrData = $arrCat; 
	}
	
	public function generate() {
		$strBuffer = parent::generate();
		
		if ($this->readonly || $this->disabled)
		   return $strBuffer;
		//Startwert aufarbeiten
		$startwert = $this->value;
		//
		$catid = 0;
		$strBuffer = '';//'<select name="'.$this->name.'" onchange="onChangeEventCat();">';
	    foreach ($this->arrData as $key => $val) {
	        if ($val != 'Tanzimpulse unterwegs') {
				if ($key == $startwert) {
					$strBuffer .= '<input type ="radio" name="'.$this->name.'" checked="checked" value="'.$key.'" onchange="onChangeEventCat();"> '.$val.'<br>';
				} else {
					$strBuffer .= '<input type ="radio" name="'.$this->name.'" value="'.$key.'" onchange="onChangeEventCat();"> '.$val.'<br>';
				}
			}
		}
		//$strBuffer .= '</select>';
		return $strBuffer;
	}
}
?>