<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */


/**
* Class ModuleSeminarCategoryList
*
* Front end module "seminar list".
* @copyright  Gerd Regnery 2013
* @author     Gerd Regnery <http://www.webdesign-impulse.de>
* @package    Seminarverwaltung
*/
class ModuleSeminarCategoryList extends SeminarEvents
{

	/**
	* Current date object
	* @var integer
	*/
	protected $Date;

	/**
	* Template
	* @var string
	*/
	protected $strTemplate = 'mod_seminar_category';
	
	/**
	* Current URL
	* @var string
	*/
	protected $strUrl;

	/**
	* Display a wildcard in the back end
	* @return string
	*/
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### SEMINAR CATEGORY LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		return parent::generate();
	}

	/**
	* Generate module
	*/
	protected function compile()
	{
		global $objPage;
		//
		// Get Seminar Categories
		//
 		$time = time();

		$strUrl = $this->strUrl;

		// Sprung zum Buchenformular ermitteln aus Modul
		$sqlBookPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$bookJumpTo = $this->Database->prepare($sqlBookPg)
          	->execute($this->sv_jumpToBuchen);
		if ($bookJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($bookJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpToBuchen;
		}
		$jumpToBuchen = $tarPage;
		
		// Sprung zum Seminarreader ermitteln aus Modul
		$sqlModPg = "SELECT id, alias FROM tl_page WHERE id=?";
		$modJumpTo = $this->Database->prepare($sqlModPg)
          	->execute($this->sv_jumpTo);
		if ($modJumpTo->numRows) {
			$tarPage = $this->generateFrontendUrl($modJumpTo->row(),$strUrl);
		} else {
			$tarPage = $this->sv_jumpTo;
		}
		$modJumpTo = $tarPage;
		//
		// Sortkey wenn kein sortindex gesetzt ist
		//
		$catKey = 0;
		//
		// Kategorien aus Modulliste auslesen
		// 
		$strCategories = '';
		$arrDebug = array();
		$arrCategories = deserialize($this->sv_seminar_category);
		$arrDebug[] = $arrCategories;
		if (empty($arrCategories)) {
			$sqlCat = "SELECT id,alias,title,sv_jumpTo,sortindex FROM tl_seminar_category WHERE 1"; 
			$objCat = $this->Database->prepare($sqlCat)->execute();
			$arrDebug[] = $objCat->numRows;
			while ($objCat->next()) {
				$arrCategories[] = $objCat->id;
			}
		}
		foreach($arrCategories as $catid) {
			$sqlCat = "SELECT id,alias,title,sv_jumpTo_category,sv_jumpTo,sortindex,teaser,details FROM tl_seminar_category WHERE id=?"; //.
//					  (!BE_USER_LOGGED_IN ? "  AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objCat = $this->Database->prepare($sqlCat)->execute($catid);
			$arrDebug[] = $objCat->numRows;
			if ($objCat->numRows) {
				$catId     = $objCat->id;
				$catTitle  = $objCat->title;
				$catAlias  = $objCat->alias;
				$catTeaser  = $objCat->teaser;
				$catDetails = $objCat->details;
				$catJumpToCat = $objCat->sv_jumpTo_category;
				$catJumpTo = $objCat->sv_jumpTo;
				$catSort   = ($objCat->sortindex ? $objCat->sortindex : $catKey);
				$catKey++;
				//
				// wenn Modul jumpTo nicht gesetzt, dann Kategorie jumpTo nutzen
				//
				if (empty($this->sv_jumpTo)) {
					// Sprung zum Seminarreader ermitteln
					$sqlPg = "SELECT id, alias FROM tl_page WHERE id=?";
					$objPg = $this->Database->prepare($sqlPg)->execute($catJumpTo);
					if ($objPg->numRows) {
						$tarPage = $this->generateFrontendUrl($objPg->row(),$strUrl);
					} else {
						$tarPage = $catJumpTo;
					}
					$jt = $tarPage;
				} else {
					$jt = $modJumpTo;
				}
				$arrDataRef = $this->getReferencesData($this,$catId);
				$arrCategory = $arrDataRef;
				// Sprung zum Seminarreader ermitteln
				$sqlPg = "SELECT id, alias FROM tl_page WHERE id=?";
				$objPgCat = $this->Database->prepare($sqlPg)->execute($catJumpToCat);
				$arrDebug[] = 'objPgCat->numRows= '.$objPgCat->numRows;
				$arrDebug[] = 'catJumpToCat= '.$catJumpToCat;
				if ($objPgCat->numRows) {
					$tarPageCat = $this->generateFrontendUrl($objPgCat->row(),$strUrl);
				} else {
					$tarPageCat = $catJumpToCat;
				}
				$jtCat = $tarPageCat;
				$arrCategory['id'] 	   = $catId;
				$arrCategory['title']  = $catTitle;
				$arrCategory['alias']  = $catAlias;
				$arrCategory['teaser'] = $catTeaser;
				$arrCategory['details']= $catDetails;
				//$arrCategory['href_category']   = $jtCat;
				//$arrCategory['href']   = $jt;
				$arrDebug[] = $arrCategory;
				$arrCategoryList[$catSort] = $arrCategory;
				$objTemplate = new FrontendTemplate($this->sv_category_template);
				$objTemplate->setData($arrCategory);
				$strCategories .= $objTemplate->parse();
			}
		}
		// sortiere Kategorien
		if (!empty($arrCategoryList)) {
			ksort($arrCategoryList);
		}
		// Pagination, sofern vorgesehen
		$i=0;
		// get date for page pagination
		$perPage = $this->perPage;
		$total = count($arrCategoryList);
		$limit = $total;
		$offset = 0;
		// Pagination
		if ($perPage > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			$offset = ($page - 1) * $perPage;
			$limit = min($perPage + $offset, $total);

			$objPagination = new Pagination($total, $perPage);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}
		$j = 0;
		unset($arrCategoriesPage);
		for ($i=$offset; $i<$limit; $i++)	{
			$arrCategoriesPage[$j] = $arrCategoryList[$i];
			$j++;
		}
		// Daten an SubTemplate $this->categories übergeben
		$this->Template->categories = $strCategories; //$objTemplate->parse();
		$this->Template->debug = $arrDebug;

	}
}

?>