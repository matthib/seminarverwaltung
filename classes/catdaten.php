<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

chdir('../../../');
define('TL_MODE', 'FE');
require_once('system/initialize.php');
class catdaten extends System {
	public function __construct() {
		$this->Import('Database');
		if (isset($_POST['catidx']) && is_string($_POST['catidx'])) {
			$catidx = htmlspecialchars($_POST['catidx']);

			$sql = "SELECT id,title FROM tl_seminar_category WHERE id=?";

			$result = $this->Database->prepare($sql)->execute($catidx);
			if ($result->numRows) {
        $specials = str_replace(array('[&]', '[&amp;]', '[lt]', '[gt]', '[nbsp]', '[-]'), array('&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&shy;'), $result->specials); 
				$jsondata = array($result->id,$result->title);
			} else {
				$noData = $GLOBALS['TL_LANG']['MSC']['seminar_noCatData'];
				$jsondata = array("","","","",$noData,"");
			}
			echo json_encode($jsondata);
		}
	}
}
header('Content-Type: text/javascript');
new catdaten();
?>