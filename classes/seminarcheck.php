<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

chdir('../../../../');
define('TL_MODE', 'FE');
require_once('system/initialize.php');
class seminarselect extends System {
	public function __construct() {
		$this->Import('Database');
		$arrData = array();
		$sqlSem = "SELECT * FROM tl_seminar WHERE id=? ORDER BY title ASC";
		$objSem = $this->Database->prepare($sqlSem)->execute(11);
		while ($objSem->next()) {
			$arrData = $objSem->row();
		}
		echo '<pre>';
		print_r($arrData);
		echo '</pre>';
		//
		/*
		$arrInsert['singleSRC'] = 1;
		$arrInsert['imageUrl'] = "files/trainyourbrain/SeniorenAktiv1.jpg";
		$sqlIns = "UPDATE tl_seminar %s WHERE id=?";
		$objIns = $this->Database->prepare($sqlIns)->set($arrInsert)->execute(11);
		*/
		echo $objIns;
	}
}
header('Content-Type: text/html');
echo 'Start...<br>';
$sc = new seminarselect();
echo 'Ende...<br>';
?>