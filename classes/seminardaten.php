<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

chdir('../../../../');
define('TL_MODE', 'FE');
require_once('system/initialize.php');
class seminardaten extends System {
	public function __construct() {
		$this->Import('Database');
		$time = time();
		if (isset($_POST['seminaridx']) && is_string($_POST['seminaridx'])) {
			$seminaridx = htmlspecialchars($_POST['seminaridx']);
			//$pos = strpos($seminaridx,'_')+1;
			//$id = substr($seminaridx,$pos);
			//$eventidx = htmlspecialchars($_POST['eventidx']);
			$categoryId = strtok($seminaridx, "_");
			$seminarId  = strtok("_");
			$eventId    = strtok("_");
			$sql = "SELECT title,teaser,details,specials,location,duration,cost,currency,reducedcost FROM tl_seminar WHERE id=?".(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			
			$arrSem = array();
			$arrEvt = array();
			$result = $this->Database->prepare($sql)->execute($seminarId);
			if ($result->numRows) {
			    $arrEvt['seminaridx'] = $result->id;
				$arrSem['title'] = $result->title;
				$arrSem['teaser'] = $this->replacePlaceholders($result->teaser);
				$arrSem['details'] = $this->replacePlaceholders($result->details);
				$arrSem['specials'] = $this->replacePlaceholders($result->specials);
				$arrSem['location'] = $result->location;
				$arrSem['duration'] = $result->duration;
				$arrSem['cost'] = $result->cost;
				$arrSem['currency'] = $result->currency;
				$arrSem['reducedcost'] = $result->reducedcost;
				$sqlEvt = "SELECT * FROM tl_seminar_events WHERE id=? AND ((date>=$time) OR (date<=$time AND endDate>=$time) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time) AND date<=$time))" . 
							(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . 
							" ORDER BY date";
				//$sqlEvt = "SELECT intern,details,date,endDate,startTime,endTime,addTime FROM tl_seminar_events WHERE id=?";
				$objEvt = $this->Database->prepare($sqlEvt)->limit(1)->execute($eventId);
				if ($objEvt->numRows) {
				    $arrEvt['eventidx'] = $objEvt->id;
					$arrEvt['intern'] = $objEvt->intern;
					$arrEvt['evtDetails'] = $this->replacePlaceholders($objEvt->details);
					$arrEvt['startDate'] = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->date);
					if ($objEvt->multipleDate) {
						$arrEvt['endDate'] = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->endDate);
					} else {
						$arrEvt['endDate'] = NULL;
					}
					if ($objEvt->addTime) {
						$arrEvt['startTime'] = $this->parseDate($GLOBALS['TL_CONFIG']['timeFormat'],$objEvt->startTime);
						$arrEvt['endTime'] = $this->parseDate($GLOBALS['TL_CONFIG']['timeFormat'],$objEvt->endTime);
					} else {
						$arrEvt['startTime'] = "";
						$arrEvt['endTime'] = "";
					}
				}
				$jsondata = array_merge($arrSem,$arrEvt);
			} else {
				$noData = $GLOBALS['TL_LANG']['MSC']['seminar_noData'];
				$jsondata = array($noData,"","","","","","","","","","","","","","","","");
			}
			echo json_encode($jsondata);
		} else if (isset($_POST['catidx']) && is_string($_POST['catidx'])) {
			$catidx = htmlspecialchars($_POST['catidx']);

			$sql = "SELECT id,title FROM tl_seminar_category WHERE id=? ".(!BE_USER_LOGGED_IN ? " AND protected=''" : "");

			$result = $this->Database->prepare($sql)->execute($catidx);
			if ($result->numRows) {
				$jsondata = array($result->id,$result->title);
			} else {
				$noData = $GLOBALS['TL_LANG']['MSC']['seminar_noCatData'];
				$jsondata = array("","","","",$noData,"");
			}
			echo json_encode($jsondata);
		}
	}
	private function replacePlaceholders($str) {
		$data = str_replace(array('[&]', '[&amp;]', '[lt]', '[gt]', '[nbsp]', '[-]'), array('&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&shy;'), $str); 
		return $data;
	}
}
header('Content-Type: text/javascript');
new seminardaten();
?>