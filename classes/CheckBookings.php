<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2015
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

//chdir('../../../../');
//define('TL_MODE', 'FE');
//require_once('system/initialize.php');
class CheckBookings extends System {
	protected $arrData = array();
	
	public function __construct() {
		//
		// Defaultwerte setzen
		//
		$arrData = array();
		$booking = 0;
		$reservation = 0;
		$storno = 0;
		$payed = 0;
		//
		// Alle Events ermitteln
		//
		$objEvents = \SeminarEventsModel::findAll();
		if ($objEvents) {
			// 
			// je Event Buchungsdaten ermitteln
			//
			foreach($objEvents as $objEvent) {
				$eventid = $objEvent->id;
				$places_booked = $objEvent->places_booked;
				// 
				// je Event die Buchungen ermitteln
				//
				$objBookedEvents = \SeminarBookingModel::findByPid($eventid);
				if ($objBookedEvents) {
					$countTotal = $objBookedEvents->count();
					$booking = 0;
					$reservation = 0;
					$storno = 0;
					$payed = 0;
					//
					// je Buchung Daten aufaddieren (Buchungen, Storno, Reservierungen, Bezahlt)
					//
					foreach ($objBookedEvents as $objBooking) {
						$booking += $objBooking->booking ? 1 : 0;
						$storno += $objBooking->storno ? 1 : 0;
						$reservation += $objBooking->reservation ? 1 : 0;
						$payed += $objBooking->payed ? 1 : 0;
					}
					$count = $booking;
				} else {
					$count = 0;
				} 
				//
				// prüfen, ob Korrektur der Eventdaten notwendig sind
				//
				$correction = $count - $places_booked;
				if ($correction != 0) {
					// Event korrigieren und LOG schreiben
					$objEvent->places_booked = $count;
					$objEvent->save();
					$strError = 'Event ID '.$eventid.' ('.$objEvent->intern.') corrected '.$correction.' to '.$count.'!';
					$this->log($strError,__METHOD__,'WARN');
				}
				// 
				// Result Array je Event füllen
				//
				$arrData[$eventid]['booked'] = $places_booked;
				$arrData[$eventid]['checked'] = $count;
				$arrData[$eventid]['result'] = ($count == $places_booked) ? 'OK ' : 'DIFF';
				$arrData[$eventid]['correction'] = $correction;
				$arrData[$eventid]['reservation'] = $reservation;
				$arrData[$eventid]['storno'] = $storno;
				$arrData[$eventid]['payed'] = $payed;
			}
		} else {
		}
		$this->arrData = $arrData;
	}
	
	public function getResult($eventid=0) {
		$arrData = array();
		// Result Array auslesen
		if ($eventid) {
			$arrData = $this->arrData[$eventid];
		} else {
			$arrData = $this->arrData;
		}
		return $arrData;
	}
}
?>