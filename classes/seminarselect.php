<?php
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Gerd Regnery 2011 - 2013
 * @author     Gerd Regnery <http://www.webdesign-impulse.de>
 * @package    Seminarverwaltung
 * @license    Commercial 
 */

chdir('../../../../');
define('TL_MODE', 'FE');
require_once('system/initialize.php');
class seminarselect extends System {
	public function __construct() {
		$this->Import('Database');
		//$this->loadLanguageFile('default');
		$intStart = time();
		$time = $intStart;
		$intEnd = strtotime("+3 Month",$intStart);
		if (isset($_POST['kategorie']) && is_string($_POST['kategorie'])) {
			$catidx = htmlspecialchars($_POST['kategorie']);
			if ($catidx == 0) {
				$sqlCat = "SELECT * FROM tl_seminar_category WHERE 1 ORDER BY title ASC";
				$objCat = $this->Database->prepare($sqlCat)->execute();
			} else {	
				$sqlCat = "SELECT * FROM tl_seminar_category WHERE id=? ORDER BY title ASC";
				$objCat = $this->Database->prepare($sqlCat)->execute($catidx);
			}
		} else {	
			$sqlCat = "SELECT * FROM tl_seminar_category WHERE 1 ORDER BY title ASC";
			$objCat = $this->Database->prepare($sqlCat)->execute();
		}
		//
		// $this->sv_show_recurring aus POST übernehmen
		//
		$blnRecurring = false;
		if (isset($_POST['show_recurring']) && is_string($_POST['show_recurring'])) {
			$blnRecurring = htmlspecialchars($_POST['show_recurring']);
		}
		//
		$arrSem = array();
	    while ($objCat->next()) {
	    	$catid    = $objCat->id;
	    	$cattitle = $objCat->title;
	    	$sqlSem = "SELECT * FROM tl_seminar WHERE pid=?".(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "");
			$objSem = $this->Database->prepare($sqlSem)->execute($catid);
			$i = 0;
 			$arrSem['0_0_0'] = $GLOBALS['TL_LANG']['MSC']['seminar_choose Seminar'];
 			while ($objSem->next()) {
    			$semid       = $objSem->id;
	    		$semtitle    = $objSem->title;
	    		$seminternal = $objSem->intern;

				$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? AND ((date>=$time) OR (date<=$time AND endDate>=$time) OR (recurring=1 AND (recurrences=0 OR repeatEnd>=$time) AND date<=$time))" . 
							(!BE_USER_LOGGED_IN ? " AND (start='' OR start<$time) AND (stop='' OR stop>$time) AND published=1" : "") . 
							" ORDER BY date";
	    		//$sqlEvt = "SELECT * FROM tl_seminar_events WHERE pid=? ORDER BY date ASC";
	    		$objEvt = $this->Database->prepare($sqlEvt)->execute($semid);
	    		if ($objEvt->numRows) { 
	    			$evtdate = $objEvt->date;
	    			$keydate = date("Ymd",$evtdate);
	    			$evtid = $objEvt->id;
					$startDt = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$evtdate);
	    			$idx = $keydate . '_' . $semid . '_' . $evtid;
					$arrSem[$idx] = $startDt . ' / '.$semtitle.' ('.$objEvt->intern.')';
					if ($blnRecurring) {
		    			if ($objEvt->recurring) {
		    				$count = 0;
							$arrRepeat = deserialize($objEvt->repeatEach);
							if (!empty($arrRepeat)) {
								$strtotime = '+ ' . $arrRepeat['value'] . ' ' . $arrRepeat['unit'];
								if ($arrRepeat['value'] < 1) {
									continue;
								}
								while ($objEvt->endTime < $intEnd) {
									if ($objEvt->recurrences > 0 && $count++ >= $objEvt->recurrences) {
										break;
									}
									$objEvt->startTime = strtotime($strtotime, $objEvt->startTime);
									$objEvt->endTime = strtotime($strtotime, $objEvt->endTime);
									// Skip events outside the scope
									if ($objEvt->endTime < $intStart || $objEvt->startTime > $intEnd)	{
										continue;
									}
					    			$keydate = date("Ymd",$objEvt->startTime);
									$startDt = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'],$objEvt->startTime);
					    			$idx = $keydate . '_' . $semid . '_' . $evtid;
									$arrSem[$idx] = $startDt . ' / '.$semtitle.' ('.$objEvt->intern.')';
				    			}
			    			}
						}
					}
	    		}
	    	}
		}
		// sortieren der Seminare nach Datum
		if (!empty($arrSem)) {
			ksort($arrSem);
		}
		//Startwert aufarbeiten
		$startwert = $this->value;
		//
		$seminarid = 0;
		$strBuffer = '<select name="seminar" onchange="onChangeEvent();">';
 		foreach ($arrSem as $key => $val) {
			if ($key == $startwert) {
				$strBuffer .= '<option selected="selected" value="'.$key.'">'.$val.'</option>';
			} else {
				$strBuffer .= '<option value="'.$key.'">'.$val.'</option>';
			}
		}
		$strBuffer .= '</select>';
		$strBuffer = str_replace('\/','/',json_encode($strBuffer));
		echo $strBuffer;
	}
}
header('Content-Type: text/javascript');
new seminarselect();
?>